package com.brainmagic.gatescatalog.activities.productsearch.fragmentoe;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.brainmagic.gatescatalog.adapter.GatesPDFListViewAdapter;
import com.brainmagic.gatescatalog.adapter.VideoResourceRecyclerAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class OtherResourceFragment extends Fragment {
    private RelativeLayout pdfLayout,videosButton;
    private ViewGroup otherResLayout;
    private LinearLayout videosLayout;
    private ImageView pdfArrow, videosArrow;
    private List<String> videoData;
    private RecyclerView videoView;
    private List<String> pdfData;
    private ListView pdfListView;
    private Alertbox alertBox;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_pdf_list,container,false);
        pdfLayout = view.findViewById(R.id.pdf_layout);
        videosButton = view.findViewById(R.id.videos_button);
        otherResLayout = view.findViewById(R.id.other_res_layout);
        pdfArrow = view.findViewById(R.id.pdf_arrow);
        videosArrow = view.findViewById(R.id.video_arrow);
//        pdfView = otherResLayout.findViewById(R.id.pdf_list);
        videosLayout = otherResLayout.findViewById(R.id.videos_layout);
        videoView = otherResLayout.findViewById(R.id.video_recycler_view);
        pdfListView = otherResLayout.findViewById(R.id.pdf_list);
        LinearLayout errorVideoLayout = view.findViewById(R.id.error_video_layout);
        LinearLayout errorPdfLayout = view.findViewById(R.id.error_pdf_layout);
        LinearLayout subOtherResLayout = view.findViewById(R.id.sub_other_res_layout);

        alertBox = new Alertbox(getActivity());

        if((videoData !=null && videoData.size()==0) && (pdfData !=null && pdfData.size()==0))
        {
            errorVideoLayout.setVisibility(View.VISIBLE);
            subOtherResLayout.setVisibility(View.GONE);
        }
        else {
            if (videoData != null && videoData.size() != 0) {
                setVideoAdapter();
                videosButton.setVisibility(View.VISIBLE);
            } else {
                videosButton.setVisibility(View.GONE);
            }

            if (pdfData != null && pdfData.size() != 0) {
                setPdfLayout();
                pdfLayout.setVisibility(View.VISIBLE);
            } else {
                pdfLayout.setVisibility(View.GONE);
                errorPdfLayout.setVisibility(View.VISIBLE);
            }
        }


        pdfLayout.setOnClickListener(new View.OnClickListener() {
            boolean isVisible;
            @Override
            public void onClick(View view) {
                ChangeBounds changeBounds=new ChangeBounds();
                changeBounds.setDuration(600L);
                TransitionManager.beginDelayedTransition(otherResLayout,changeBounds);
                isVisible = !isVisible;
                if(isVisible)
                    pdfArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    pdfArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                pdfListView.setVisibility(isVisible?View.VISIBLE:View.GONE);
            }
        });

        videosButton.setOnClickListener(new View.OnClickListener() {
            boolean isVisible;
            @Override
            public void onClick(View view) {
                ChangeBounds changeBounds=new ChangeBounds();
                changeBounds.setDuration(600L);
                TransitionManager.beginDelayedTransition(otherResLayout,changeBounds);
                isVisible = !isVisible;
                if(isVisible)
                    videosArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    videosArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                videoView.setVisibility(isVisible?View.VISIBLE:View.GONE);
            }
        });

        pdfListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String pdfModel = (String) adapterView.getAdapter().getItem(i);
                try {
                    if(pdfModel != null) {
                        Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(pdfModel));
                        if (searchAddress != null && !TextUtils.isEmpty(pdfModel))
                            startActivity(searchAddress);
                        else {
                            alertBox.showAlert("No Link Found");
                        }
                    }
                }catch (ActivityNotFoundException e)
                {
                    alertBox.showAlert("No Link found to Open");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout,"No Link found to Open", Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
                }
                catch (Exception e)
                {
                    alertBox.showAlert("Invalid Link");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout, "Invalid Link", Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
//                    Log.d(TAG, "onClick: "+e);
                }
            }
        });

        return view;
    }

    //    public void setResourceData(List<VideoAttachment> videoData, List<PDFModel> pdfData) {
    public void setResourceData(List<String> videoData, List<String> pdfData) {
        this.videoData = videoData;
        this.pdfData = pdfData;
    }

    private void setVideoAdapter()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        VideoResourceRecyclerAdapter adapter = new VideoResourceRecyclerAdapter(getActivity(),videoData);

        videoView.setLayoutManager(linearLayoutManager);
        videoView.setAdapter(adapter);
    }

    private void setPdfLayout()
    {
        GatesPDFListViewAdapter adapter =new GatesPDFListViewAdapter(getContext(),pdfData);
        pdfListView.setAdapter(adapter);
    }
}
