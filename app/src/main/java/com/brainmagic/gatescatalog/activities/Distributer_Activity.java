package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.Disributer_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.StringListModel;
import com.brainmagic.gatescatalog.api.models.distributors.DistributorsListModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Distributer_Activity extends Activity {
    ImageView menu;
    FloatingActionButton back,home;
    Spinner state_spinner, city_spinner, country_spinner;
    String state_state = "All", city_city = "All", country_country = "All";
    String query;
    SQLiteDatabase db;
    ListView listView;
    Cursor c;

    private Alertbox alertbox;
    String[] calls = new String[50];

    ArrayList<String> state;
    ArrayList<String> city, country;
    ArrayList<String> distributer;
    ArrayList<String> contact_person;
    ArrayList<String> adress;
    ArrayList<String> landline;
    ArrayList<String> mobno, pincode, email, country_list, state_list, city_list;
    ArrayList<String> latitude;
    ArrayList<String> longitude;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Email, Mobno, Usertype;
    String key;
    TextView textView2;
    Disributer_Adapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributer_);

        back =  findViewById(R.id.back);
        home =  findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        listView = (ListView) findViewById(R.id.list_view);

        textView2 = (TextView) findViewById(R.id.textView2);


        state_spinner = (Spinner) findViewById(R.id.state_spinner);
        city_spinner = (Spinner) findViewById(R.id.city_spinner);
        country_spinner = (Spinner) findViewById(R.id.country_spinner);

        alertbox = new Alertbox(this);

        state = new ArrayList<String>();
        city = new ArrayList<String>();
        country = new ArrayList<String>();

        state_list = new ArrayList<String>();
        country_list = new ArrayList<String>();
        city_list = new ArrayList<String>();

        distributer = new ArrayList<String>();
        contact_person = new ArrayList<String>();
        adress = new ArrayList<String>();
        landline = new ArrayList<String>();
        mobno = new ArrayList<String>();
        latitude = new ArrayList<String>();
        longitude = new ArrayList<String>();
        pincode = new ArrayList<String>();
        email = new ArrayList<String>();

        key = getIntent().getStringExtra("KEY");


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("Email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");

//        textView2.setText(key);

        callApiServiceForDistributionCountryList();


        country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                country_country = adapterView.getItemAtPosition(i).toString();


                //  GetcityssNames();
                if (country_country.equals("ALL")) {
                    state.clear();
                    city.clear();
                    state.add("ALL");
                    city.add("ALL");
                    state_spinner.setAdapter(new ArrayAdapter<String>(Distributer_Activity.this, android.R.layout.simple_list_item_1, state));
                    city_spinner.setAdapter(new ArrayAdapter<String>(Distributer_Activity.this, android.R.layout.simple_list_item_1, city));
                    Log.v("ctry_country_all", "Select * From Contact_Details where Key='" + key + "'");
//                    new Distributer().execute("Select * From Contact_Details where Key='" + key + "' order by Name asc");
                    callApiServiceForDistributionSearchList();
//                    callApiServiceForDistributionSearchList();

                    //Log.v("country_all", "Select * From Contact_Details");
                } else {
//                    GetStatesNames();
                    callApiServiceForDistributionStateList(country_country);
                    callApiServiceForDistributionSearchList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                state_state = adapterView.getItemAtPosition(i).toString();


                if (!country_country.equals("ALL")) {
                    if (state_state.equals("ALL")) {

                        city.clear();
                        city.add("ALL");
                        city_spinner.setAdapter(new ArrayAdapter<String>(Distributer_Activity.this, android.R.layout.simple_list_item_1, city));
                        //  if (!state_state.equals("ALL"))
                        //  new Distributer().execute("Select * From Contact_Details where Country='" + country_country + "' and Key='"+key+"'");
                        callApiServiceForDistributionCityList(country_country,state_state);
                        callApiServiceForDistributionSearchList();
                        // new Distributer().execute("Select * From Dis_Excess_DATA");
                    } else if (!state_state.equals("ALL")) {
//                        GetcityssNames();
                        callApiServiceForDistributionCityList(country_country,state_state);
                        callApiServiceForDistributionSearchList();
                        //  new Distributer().execute("Select * From Contact_Details where Country='"+country_country+"' and State='"+state_state+"'and Key='"+key+"'");
                    } else {
                        // Log.v("not_all_all", "Select * From Contact_Details where Country='" + country_country + "' and State='" + state_state + "'and City='"+ city_city +"'");
                        // new Distributer().execute("Select * From Contact_Details where Country='" + country_country + "' and State='" + state_state + "' and City='"+city_city+"'");
                        Log.v("state_not_all_all", "Select * From Contact_Details where Key='" + key + "'");
//                        new Distributer().execute("Select * From Contact_Details where Key='" + key + "'order by Name asc");
                    }

                }


                // query = "select State,City,Distributer,Contact_Person,Adress,Landline_No,Mob_No from Dis_Distributer";

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                city_city = adapterView.getItemAtPosition(i).toString();

                if (!country_country.equals("ALL")) {
                    if (state_state.equals("ALL") && city_city.equals("ALL")) {
                        //  if (!state_state.equals("ALL"))
                        Log.v("cctry_not_all state_all", "Select * From Contact_Details where Country='" + country_country + "' and Key='" + key + "'");
//                        new Distributer().execute("Select * From Contact_Details where Country='" + country_country + "' and Key='" + key + "'order by Name asc");
                        callApiServiceForDistributionSearchList();
                        // new Distributer().execute("Select * From Dis_Excess_DATA");
                    } else if (!state_state.equals("ALL") && city_city.equals("ALL")) {
                        Log.v("cctry,ste_not_all", "Select * From Contact_Details where Country='" + country_country + "' and State='" + state_state + "' and Key='" + key + "'");
//                        new Distributer().execute("Select * From Contact_Details where Country='" + country_country + "' and State='" + state_state + "' and Key='" + key + "'order by Name asc");
                        callApiServiceForDistributionSearchList();
                    } else {
                        // Log.v("not_all_all", "Select * From Dis_Excess_DATA where state='" + state_state + "' and city='" + city_city + "'");
                        Log.v("cnot_all_all", "Select * From Contact_Details where Country='" + country_country + "' and State='" + state_state + "' and City='" + city_city + "' and Key='" + key + "'");
//                        new Distributer().execute("Select * From Contact_Details where Country='" + country_country + "' and State='" + state_state + "' and City='" + city_city + "' and Key='" + key + "'order by Name asc");
                        callApiServiceForDistributionSearchList();
                    }
                } else {
                    city_spinner.setSelection(0);
                    Log.v("call_all", "Select * From Contact_Details where Key='" + key + "'");
                    // new Distributer().execute("Select * From Contact_Details where Key='"+key+"'order by Name asc");
                }


                //   new STATE().execute(query);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        GetCountrysNames();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(Distributer_Activity.this,HomeScreenActivity.class)
               .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK)
               );
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Distributer_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Distributer_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Distributer_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Distributer_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Distributer_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Distributer_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(Distributer_Activity.this, ScanActivity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Distributer_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Distributer_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Distributer_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Distributer_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Distributer_Activity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Distributer_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Distributer_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

       checkinternet();

    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(Distributer_Activity.this);

        if (connection.CheckInternet())
        {
//            new BackroundRunning().execute();
            callApiServiceForDistributionList();

        } else
        {
            // Toast.makeText(Notification_Activity.this, "NO INTERNATE", Toast.LENGTH_SHORT).show();
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }


    }

    private void callApiServiceForDistributionCountryList()
    {
        try {
            APIService service = RetrofitClient.getApiService();
            Call<StringListModel> call = service.getDistributorCountryList();

            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {

                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            country.clear();
                            country.add("ALL");
                            country.addAll(response.body().getData());
                            ArrayAdapter country_spinner_adapter = new ArrayAdapter(Distributer_Activity.this, android.R.layout.simple_list_item_1, country);
                            country_spinner.setAdapter(country_spinner_adapter);
                        }
                        else {
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

    private void callApiServiceForDistributionSearchList()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Distributer_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();
            Call<DistributorsListModel> call = service.getDistributorList(country_country,state_state,city_city);

            call.enqueue(new Callback<DistributorsListModel>() {
                @Override
                public void onResponse(Call<DistributorsListModel> call, Response<DistributorsListModel> response) {
                    progressDialog.dismiss();

                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            adapter.setDistributorListResults(response.body().getData());
                            adapter.notifyDataSetChanged();
                        }
                        else {
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<DistributorsListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

    private void callApiServiceForDistributionStateList(String country_country)
    {
        try {
            APIService service = RetrofitClient.getApiService();
            Call<StringListModel> call = service.getDistributorStateList(country_country);

            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {

                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            state_list.clear();
                            state_list.add("ALL");
                            state.addAll(response.body().getData());

                            ArrayAdapter state_spinner_adapter = new ArrayAdapter(Distributer_Activity.this, android.R.layout.simple_list_item_1, state);

                            state_spinner.setAdapter(state_spinner_adapter);
                        }
                        else {
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

    private void callApiServiceForDistributionCityList(String country_country, String state_state)
    {
        try {
            APIService service = RetrofitClient.getApiService();
            Call<StringListModel> call = service.getDistributorCityList(country_country,state_state);

            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {

                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            city.clear();
                            city.add("ALL");
                            city.addAll(response.body().getData());

                            ArrayAdapter city_spinner_adapter = new ArrayAdapter(Distributer_Activity.this, android.R.layout.simple_list_item_1, city);

                            city_spinner.setAdapter(city_spinner_adapter);
                        }
                        else {
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

    private void callApiServiceForDistributionList()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Distributer_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();
            Call<DistributorsListModel> call = service.getDistributorList(key);

            call.enqueue(new Callback<DistributorsListModel>() {
                @Override
                public void onResponse(Call<DistributorsListModel> call, Response<DistributorsListModel> response) {
                    progressDialog.dismiss();
                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            adapter = new Disributer_Adapter(Distributer_Activity.this,response.body().getData()
                            );
                            listView.setAdapter(adapter);
                        }
                        else {
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<DistributorsListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

//    private void GetcityssNames() {
//        city.clear();
//        city.add("ALL");
//
//        String selectQuery = "";
//
//
//        DBHelper dbHelper = new DBHelper(Distributer_Activity.this);
//        db = dbHelper.readDataBase();
//
//
//        // String selectQuery = "SELECT Oem_list,id FROM Oem_Table";
//
//        //  String selectQuery = "SELECT * FROM Vehicle_Table";
//// if (Usertype.equals("Others")|(Usertype.equals("Gates Employee"))){
//        if (!country_country.equals("ALL") && !state_state.equals("ALL")) {
//
//            selectQuery = "select distinct City from Contact_Details where Country='" + country_country + "' and State='" + state_state + "' and Key='" + key + "'order by City asc";
//        } else if (!country_country.equals("ALL") && state_state.equals("ALL")) {
//            selectQuery = "select distinct City from Contact_Details where Country='" + country_country + "' and Key='" + key + "'order by City asc";
//        } else if (country_country.equals("ALL") && state_state.equals("ALL")) {
//            selectQuery = "select distinct City from Contact_Details where Key='" + key + "'order by City asc";
//        }
//
//        c = db.rawQuery(selectQuery, null);
//        if (c.moveToFirst()) {
//            do {
//                city.add(c.getString(c.getColumnIndex("City")));
//                //   Image_name.add(c.getString(c.getColumnIndex("Image_Logo")));
//
//            }
//            while (c.moveToNext());
//
//        }
//        c.close();
//
//
//        ArrayAdapter city_spinner_adapter = new ArrayAdapter(Distributer_Activity.this, android.R.layout.simple_list_item_1, city);
//
//        city_spinner.setAdapter(city_spinner_adapter);
//
//    }

//    private void GetStatesNames() {
//
//        state.clear();
//        state.add("ALL");
//
//        DBHelper dbHelper = new DBHelper(Distributer_Activity.this);
//        db = dbHelper.readDataBase();
//
//
//        String selectQuery = "";
//
//        //  String selectQuery = "SELECT * FROM Vehicle_Table";
//        if (!country_country.equals("ALL")) {
//            selectQuery = "select distinct State from Contact_Details where Key='" + key + "' and Country='" + country_country + "' order by State asc";
//        } else {
//            selectQuery = "select distinct State from Contact_Details where Key='" + key + "' order by State asc ";
//        }
//        c = db.rawQuery(selectQuery, null);
//        if (c.moveToFirst()) {
//            do {
//                state.add(c.getString(c.getColumnIndex("State")));
//                //   Image_name.add(c.getString(c.getColumnIndex("Image_Logo")));
//
//            }
//            while (c.moveToNext());
//
//        }
//        c.close();
//
//
//        ArrayAdapter state_spinner_adapter = new ArrayAdapter(Distributer_Activity.this, android.R.layout.simple_list_item_1, state);
//
//        state_spinner.setAdapter(state_spinner_adapter);
//
//    }

//    private void GetCountrysNames() {
//
//        country.clear();
//        country.add("ALL");
//
//        DBHelper dbHelper = new DBHelper(Distributer_Activity.this);
//        db = dbHelper.readDataBase();
//
//
//        // String selectQuery = "SELECT Oem_list,id FROM Oem_Table";
//
//        //  String selectQuery = "SELECT * FROM Vehicle_Table";
//
//        String selectQuery = "select distinct Country from Contact_Details where Key='" + key + "' order by Country asc ";
//        c = db.rawQuery(selectQuery, null);
//        if (c.moveToFirst()) {
//            do {
//                country.add(c.getString(c.getColumnIndex("Country")));
//                //   Image_name.add(c.getString(c.getColumnIndex("Image_Logo")));
//
//            }
//            while (c.moveToNext());
//
//        }
//        c.close();
//
//        ArrayAdapter country_spinner_adapter = new ArrayAdapter(Distributer_Activity.this, android.R.layout.simple_list_item_1, country);
//
//        country_spinner.setAdapter(country_spinner_adapter);
//
//    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }



//    class Distributer extends AsyncTask<String, Void, String> {
//        ProgressDialog loading;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            loading = ProgressDialog.show(Distributer_Activity.this, "Loading", "Please wait...", false, false);
//
//            distributer.clear();
//            contact_person.clear();
//            adress.clear();
//            landline.clear();
//            mobno.clear();
//            latitude.clear();
//            longitude.clear();
//            pincode.clear();
//            email.clear();
//            state_list.clear();
//            city_list.clear();
//            country_list.clear();
//
//
//            if (adapter != null)
//                adapter.clear();
//
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//
//            DBHelper dbHelper = new DBHelper(Distributer_Activity.this);
//            db = dbHelper.readDataBase();
//
//
//            // String selectQuery = "SELECT Oem_list,id FROM Oem_Table";
//
//            //  String selectQuery = "SELECT * FROM Vehicle_Table";
//
//            c = db.rawQuery(strings[0], null);
//            if (c.moveToFirst()) {
//                do {
//
//                    distributer.add(c.getString(c.getColumnIndex("Name")));
//                    contact_person.add(c.getString(c.getColumnIndex("Contact_person")));
//                    adress.add(c.getString(c.getColumnIndex("Adress")));
//                    landline.add(c.getString(c.getColumnIndex("Landline_No")));
//                    mobno.add(c.getString(c.getColumnIndex("Mob_No")));
//                    pincode.add(c.getString(c.getColumnIndex("Pincode")));
//                    email.add(c.getString(c.getColumnIndex("Email")));
//                    latitude.add(c.getString(c.getColumnIndex("Latitude")));
//                    longitude.add(c.getString(c.getColumnIndex("Longitude")));
//                    country_list.add(c.getString(c.getColumnIndex("Country")));
//                    state_list.add(c.getString(c.getColumnIndex("State")));
//                    city_list.add(c.getString(c.getColumnIndex("City")));
//
//
//                    //   Image_name.add(c.getString(c.getColumnIndex("Image_Logo")));
//
//                }
//
//                while (c.moveToNext());
//            }
//
//
//            c.close();
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            loading.dismiss();
//            adapter = new Disributer_Adapter(Distributer_Activity.this, state_list, city_list, country_list, distributer, contact_person, adress, landline, mobno, latitude, longitude, pincode, email);
//            listView.setAdapter(adapter);
//        }
//
//    }


}






