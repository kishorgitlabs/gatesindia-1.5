package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.ListViewAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.StringListModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Distributor_Search_Activity extends Activity {


    ImageView menu;
    FloatingActionButton back,home;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Email, Mobno, Usertype;
    ListView listView;
    SQLiteDatabase db;
    Cursor c;
    List<String> key, logo;
    ArrayList<Bitmap> list_logo;
    Bitmap bitmap;
    ListViewAdapter adapter;
    private Alertbox alertbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact__search_);

        back =  findViewById(R.id.back);
        home =  findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        listView = (ListView) findViewById(R.id.list_view);


        alertbox = new Alertbox(this);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("Email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");


        key = new ArrayList<String>();
        logo = new ArrayList<String>();
        list_logo = new ArrayList<Bitmap>();


//        new Oemnames().execute();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Distributor_Search_Activity.this,HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Distributor_Search_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Distributor_Search_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Distributor_Search_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Distributor_Search_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Distributor_Search_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Distributor_Search_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(Distributor_Search_Activity.this, ScanActivity.class));
                                return true;

                            case R.id.pricelist:
                                startActivity(new Intent(Distributor_Search_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Distributor_Search_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Distributor_Search_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Distributor_Search_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Distributor_Search_Activity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Distributor_Search_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Distributor_Search_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a = new Intent(Distributor_Search_Activity.this, Distributer_Activity.class);
                a.putExtra("KEY", key.get(i).toString());
                startActivity(a);
            }
        });


        checkinternet();
    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(Distributor_Search_Activity.this);

        if (connection.CheckInternet())
        {
//            new BackroundRunning().execute();
            callApiServiceForDistributionList();

        } else
        {
            // Toast.makeText(Notification_Activity.this, "NO INTERNATE", Toast.LENGTH_SHORT).show();
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }


    }

    private void callApiServiceForDistributionList()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Distributor_Search_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();
            Call<StringListModel> call = service.getDistributorNameList();

            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {
                    progressDialog.dismiss();
                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            key = response.body().getData();
                            adapter = new ListViewAdapter(Distributor_Search_Activity.this,response.body().getData());
                            listView.setAdapter(adapter);
                        }
                        else {
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//    class Oemnames extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... strings) {
//try {
//    DBHelper dbHelper = new DBHelper(Distributor_Search_Activity.this);
//    db = dbHelper.readDataBase();
//
//
//    // String selectQuery = "SELECT Oem_list,id FROM Oem_Table";
//
//    //  String selectQuery = "SELECT * FROM Vehicle_Table";
//
//    String selectQuery = "select distinct key,Logo from Contact_Details";
//    c = db.rawQuery(selectQuery, null);
//    if (c.moveToFirst()) {
//        do {
//            key.add(c.getString(c.getColumnIndex("key")));
//            logo.add(c.getString(c.getColumnIndex("Logo")));
//        }
//        while (c.moveToNext());
//
//    }
//    c.close();
//}catch (Exception e){
//    e.printStackTrace();
//}
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            try {
//                for (int i = 0; i < logo.size(); i++) {
//                    list_logo.add(downloadImage(logo.get(i) + ".jpg"));
//                }
//
//                Contact_Adapter adapter;
//                adapter = new Contact_Adapter(Distributor_Search_Activity.this, key, list_logo);
//
//                listView.setAdapter(adapter);
//           /* listView.setAdapter(new ArrayAdapter<String>(Distributor_Search_Activity.this,
//                    android.R.layout.simple_list_item_1, vehicle));*/
//            }catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//
//        private Bitmap downloadImage(String url) {
//
//            try {
//                InputStream fileInputStream = Distributor_Search_Activity.this.getAssets().open(url);
////                         Options options = new Options();
////                         options.inScaled = false;
////                         options.inSampleSize = 8;
//                bitmap = BitmapFactory.decodeStream(fileInputStream);
//                fileInputStream.close();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.v("Error", e.getMessage());
//                return null;
//            }
//            return bitmap;
//        }
//    }

//    private void setListView()
//    {
//        List<String> distributorList = new ArrayList<>();
//        distributorList.add("Lucas");
//        adapter = new Distributor_Search_Adapter(Distributor_Search_Activity.this,distributorList);
//        listView.setAdapter(adapter);
//    }
}

