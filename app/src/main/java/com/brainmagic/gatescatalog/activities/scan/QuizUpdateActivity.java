package com.brainmagic.gatescatalog.activities.scan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.adapter.QuizRecyclerAdapter;
import com.brainmagic.gatescatalog.adapter.QuizUpdateAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.QuizGetClass;
import com.brainmagic.gatescatalog.api.models.whatsnew.QuizPojo;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizUpdateActivity extends AppCompatActivity {

    Alertbox alertbox;
    ArrayList arrayList;
    String mobno;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    ArrayList values;

    Button submit;
    List<String> lists;
    RecyclerView listview;
    ArrayList list;
  String date;
  TextView setdate;
  List getArrayList;
    JSONArray dataArr;
    String selectedanswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_update);

        values=new ArrayList();
        getArrayList=new ArrayList();
        setdate=findViewById(R.id.setdate);
        lists = new ArrayList<>();
        submit = findViewById(R.id.submit);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        alertbox = new Alertbox(QuizUpdateActivity.this);
        arrayList = new ArrayList();
        list = new ArrayList();
        listview = findViewById(R.id.listview);
        mobno = myshare.getString("MobNo", "");
        checkInternet();
      date=getIntent().getStringExtra("date");
        setdate.setText("Thanks for participating quiz,Next quiz on "+ date);
        dataArr = new JSONArray();



        for (int i = 0; i < QuizRecyclerAdapter.selectedAnswers.size(); i++) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("Question_no",QuizRecyclerAdapter.Selectedsno.get(i));
                obj.put("Question_E",QuizRecyclerAdapter.Selectedquestion.get(i));
                obj.put("Correct_answer_E",QuizRecyclerAdapter.selectedAnswers.get(i));
                dataArr.put(obj);
                values.add(QuizRecyclerAdapter.selectedAnswers.get(i));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(QuizUpdateActivity.this,LoyaltyPoitsActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
    private void checkInternet() {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(QuizUpdateActivity.this);
        if (network_connection_activity.CheckInternet()) {
            getcorrecctanswer();

        } else {
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    private void getcorrecctanswer() {
        try {
            APIService service = RetrofitClient.getApiService();
            Call<QuizPojo> call = service.getquiz(mobno);

            call.enqueue(new Callback<QuizPojo>() {
                @Override
                public void onResponse(Call<QuizPojo> call, Response<QuizPojo> response) {
                    try {
                        if (response.body().getResult().equals("Quiz List Available")) {

                            QuizUpdateAdapter adapter=new QuizUpdateAdapter(QuizUpdateActivity.this,response.body().getData(),getArrayList);
                            listview.setHasFixedSize(true);
                            listview.setLayoutManager(new LinearLayoutManager(QuizUpdateActivity.this));
                            listview.setAdapter(adapter);

                        } else if (response.body().getResult().equals("NotSuccess")){
                            alertbox.showAlertWithBack("No Record Found");

                        } else {
                            alertbox.showAlertWithBack("Could not Connect to Server. Please try again Later");
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<QuizPojo> call, Throwable t) {
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlertWithBack(e.getMessage());
        }

    }
}