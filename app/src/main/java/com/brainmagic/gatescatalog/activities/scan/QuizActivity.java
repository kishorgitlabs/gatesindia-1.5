package com.brainmagic.gatescatalog.activities.scan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.EngineSpecActivity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.EngineSpecAdapter;
import com.brainmagic.gatescatalog.adapter.PriceListAdapter;
import com.brainmagic.gatescatalog.adapter.QuizAdapter;
import com.brainmagic.gatescatalog.adapter.QuizRecyclerAdapter;
import com.brainmagic.gatescatalog.adapter.QuizRecyclerinterface;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;

import com.brainmagic.gatescatalog.api.models.PriceList.PricePojo;
import com.brainmagic.gatescatalog.api.models.QuizGetClass;
import com.brainmagic.gatescatalog.api.models.QuizModelClass;


import com.brainmagic.gatescatalog.api.models.QuizUpdatePojo;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeModel;
import com.brainmagic.gatescatalog.api.models.whatsnew.QuizPojo;
import com.brainmagic.gatescatalog.font.ButtonFontDesign;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizActivity extends AppCompatActivity {

    ImageView menu;
    FloatingActionButton home, back;
    RecyclerView listview;
    ArrayList list;
    Alertbox alertbox;
    ArrayList arrayList;
    String mobno;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    ArrayList values[];
    String getstate,getcity,getname;
   Button submit;
    List<String> lists;
    JSONArray dataArr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

         lists=new ArrayList<>();
        submit = findViewById(R.id.submit);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        alertbox = new Alertbox(QuizActivity.this);
        arrayList = new ArrayList();
        list = new ArrayList();
        menu = findViewById(R.id.menu);
        listview = findViewById(R.id.listview);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        mobno = myshare.getString("MobNo", "");
        getstate = myshare.getString("state", "");
        getname = myshare.getString("NameCheck", "");
        getcity = myshare.getString("city", "");

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(QuizActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(QuizActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub

                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(QuizActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(QuizActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(QuizActivity.this, Price_Activity.class));
                                return true;
//                            case R.id.ptoductpop:
////                                startActivity(new Intent(ScanHistory.this, ScanHistory.class));
//                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(QuizActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanHistory.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(QuizActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(QuizActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(QuizActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(QuizActivity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanHistory.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(QuizActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

        checkInternet();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                 dataArr = new JSONArray();
                for (int i = 0; i < QuizRecyclerAdapter.selectedAnswers.size(); i++) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("Question_no",QuizRecyclerAdapter.Selectedsno.get(i));
                        obj.put("Question_E",QuizRecyclerAdapter.Selectedquestion.get(i));
                        obj.put("Correct_answer_E",QuizRecyclerAdapter.selectedAnswers.get(i));
                        dataArr.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                final CustomProgressDialog progressDialog =new CustomProgressDialog(QuizActivity.this);
                progressDialog.setCancelable(false);
                progressDialog.show();
                APIService service= RetrofitClient.getApiService();

                try {
                    Call<QuizUpdatePojo> call = service.quizupdate(mobno,getstate,getname,getcity,dataArr);

                    call.enqueue(new Callback<QuizUpdatePojo>() {
                        @Override
                        public void onResponse(Call<QuizUpdatePojo> call, Response<QuizUpdatePojo> response) {
                            progressDialog.dismiss();

                            try{
                                if(response.isSuccessful()){
                                    if(response.body().getResult().equals("Success")) {
                                        String[] date=response.body().getData().getUpcomingquizdate().split("T");
//                                     alertbox.showAlertWithBack("Thanks for participating quiz, Next Quiz on"  +date[0]);
                                        Intent intent = new Intent(QuizActivity.this, QuizUpdateActivity.class);
                                        intent.putExtra("date",date[0]);
                                        startActivity(intent);
                                        finish();

                                    }
                                    else if (response.body().getResult().equals("NotSuccess")){
                                        alertbox.showAlertWithBack("Something went wrong .Please try again later");
                                    }
                                }
                            }catch (Exception e)
                            {
                                e.printStackTrace();
                                alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
                            }
                        }

                        @Override
                        public void onFailure(Call<QuizUpdatePojo> call, Throwable t) {
                            progressDialog.dismiss();
                            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
                        }
                    });
                }catch (Exception e)
                {
                    e.printStackTrace();
                    alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
                }


            }

        });

    }


    private void checkInternet() {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(QuizActivity.this);
        if (network_connection_activity.CheckInternet()) {
            getQuizQuestions();

        } else {
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    private void getQuizQuestions() {
        try {
            APIService service = RetrofitClient.getApiService();
            Call<QuizPojo> call = service.getquiz(mobno);

            call.enqueue(new Callback<QuizPojo>() {
                @Override
                public void onResponse(Call<QuizPojo> call, Response<QuizPojo> response) {
                    try {
                            if (response.body().getResult().equals("Quiz List Available")) {


                              QuizRecyclerAdapter adapter=new QuizRecyclerAdapter(QuizActivity.this,response.body().getData());
                                listview.setHasFixedSize(true);
                                listview.setLayoutManager(new LinearLayoutManager(QuizActivity.this));
                                listview.setAdapter(adapter);

                            } else if (response.body().getResult().equals("NotSuccess")){
                                alertbox.showAlertWithBack("No Record Found");

                        } else {
                            alertbox.showAlertWithBack("Could not Connect to Server. Please try again Later");
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<QuizPojo> call, Throwable t) {
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlertWithBack(e.getMessage());
        }

    }

}