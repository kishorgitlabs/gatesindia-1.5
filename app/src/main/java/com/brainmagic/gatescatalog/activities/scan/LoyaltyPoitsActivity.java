package com.brainmagic.gatescatalog.activities.scan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.PriceListAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.GatesEmployeeLogin.GatesLoginPojo;
import com.brainmagic.gatescatalog.api.models.GetMechaniScores;
import com.brainmagic.gatescatalog.api.models.PriceList.PricePojo;
import com.brainmagic.gatescatalog.api.models.QuizcheckPojo;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoyaltyPoitsActivity extends AppCompatActivity {

    ImageView menu;
     FloatingActionButton home, back;
  RelativeLayout quiz,pointshistory,tophistory,contactus;
TextView mechanicname,textpoints;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String getName,getPoints,mechmobno;
    Alertbox alertbox;
    ListView list_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loyalty_poits);
        quiz=findViewById(R.id.quiz);

        alertbox=new Alertbox(LoyaltyPoitsActivity.this);
        textpoints=findViewById(R.id.textpoints);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        mechanicname=findViewById(R.id.mechanicname);
        contactus=findViewById(R.id.contactus);
         menu=findViewById(R.id.menu);
        pointshistory=findViewById(R.id.pointshistory);
        tophistory=findViewById(R.id.tophistory);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);

        mechmobno=myshare.getString("MobNo","");
        getName=myshare.getString("NameCheck","");
        mechanicname.setText(getName);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(LoyaltyPoitsActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(LoyaltyPoitsActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub

                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(LoyaltyPoitsActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(LoyaltyPoitsActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(LoyaltyPoitsActivity.this, Price_Activity.class));
                                return true;
//                            case R.id.ptoductpop:
////                                startActivity(new Intent(ScanHistory.this, ScanHistory.class));
//                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(LoyaltyPoitsActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanHistory.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(LoyaltyPoitsActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(LoyaltyPoitsActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(LoyaltyPoitsActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(LoyaltyPoitsActivity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanHistory.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(LoyaltyPoitsActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

        contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoyaltyPoitsActivity.this,MechanicContactUs.class);
                startActivity(intent);
            }
        });
        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           checkinternets();
            }
        });
        pointshistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoyaltyPoitsActivity.this,PointsDetails.class);
                startActivity(intent);
            }
        });
        tophistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoyaltyPoitsActivity.this,TopPointsActivity.class);
                startActivity(intent);
            }
        });


        checkinternet();
    }
    private void checkinternets() {
        Network_Connection_Activity connection = new Network_Connection_Activity(LoyaltyPoitsActivity.this);

        if (connection.CheckInternet()) {

            getalldetails();
        } else {
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(LoyaltyPoitsActivity.this);

        if (connection.CheckInternet()) {
            getpoints();

        } else {
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    private void getpoints() {
        final CustomProgressDialog progressDialog = new CustomProgressDialog(LoyaltyPoitsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            APIService service = RetrofitClient.getApiService();
            Call<GetMechaniScores> call = service.getscores(mechmobno);

            call.enqueue(new Callback<GetMechaniScores>() {
                @Override
                public void onResponse(Call<GetMechaniScores> call, Response<GetMechaniScores> response) {
                    progressDialog.dismiss();
                    try {
                        if (response.body().getResult().equals("Success")) {
                            progressDialog.dismiss();
                                 int score=response.body().getData();
                                 String s=String.valueOf(score);
                                 textpoints.setText(s);
                        }
                        else if (response.body().getResult().equals("NotSuccess")){
                            progressDialog.dismiss();
                            textpoints.setText(0);
                        }
                        else {
                            progressDialog.dismiss();
                            textpoints.setText(0);
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                 }
                }

                @Override
                public void onFailure(Call<GetMechaniScores> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }


    private void getalldetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(LoyaltyPoitsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetrofitClient.getApiService();

        try {
            Call<QuizcheckPojo> call = service.quizcheck(mechmobno);
            call.enqueue(new Callback<QuizcheckPojo>() {
                @Override
                public void onResponse(Call<QuizcheckPojo> call, Response<QuizcheckPojo> response) {
                    progressDialog.dismiss();
                    try{

                            if(response.body().getResult().equals("Success")) {
                               Intent intent=new Intent(LoyaltyPoitsActivity.this,QuizActivity.class);
                               startActivity(intent);
                               finish();
                            }
                            else if (response.body().getResult().equals("Quiz Already Attended")){

                           alertbox.showAlert("You already participated in this week's quiz.\n Good luck with next week's quiz");

                            }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<QuizcheckPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }
}