package com.brainmagic.gatescatalog.activities.productsearch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.fragment.ModelSuitsFragment;
import com.brainmagic.gatescatalog.activities.productsearch.fragment.OtherResourcesFragment;
import com.brainmagic.gatescatalog.activities.productsearch.fragment.ProductSpecFragment;
import com.brainmagic.gatescatalog.activities.productsearch.fragmentoe.ProductSpeFragment;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.ViewPagerAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.google.android.material.tabs.TabLayout;

public class MoreDetailsOeActivity extends AppCompatActivity {

    private ImageView menu;
    private Alertbox alertBox;
    private ViewPagerAdapter adapter;
    private ViewPager pager;

    private TextView navicationtxt;
    private TabLayout layouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_details_oe);
        layouts = findViewById(R.id.tabs);
        alertBox = new Alertbox(MoreDetailsOeActivity.this);
        pager = findViewById(R.id.viewPager);
        navicationtxt = findViewById(R.id.navicationtxt);
        menu = findViewById(R.id.menu);

        navicationtxt.setText(getIntent().getStringExtra("navtxt"));

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        FragmentManager manager=getSupportFragmentManager();
        adapter=new ViewPagerAdapter(manager);

        checkInternet();

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(MoreDetailsOeActivity.this, view);


                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(MoreDetailsOeActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(MoreDetailsOeActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(MoreDetailsOeActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(MoreDetailsOeActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(MoreDetailsActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(MoreDetailsOeActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(MoreDetailsOeActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(MoreDetailsOeActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(MoreDetailsOeActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(MoreDetailsOeActivity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(MoreDetailsActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(MoreDetailsOeActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });
    }

    private void checkInternet() {
        Network_Connection_Activity connection_activity =new Network_Connection_Activity(MoreDetailsOeActivity.this);
        if(connection_activity.CheckInternet())
        {
            callApiForOEMoreDetails();
        }
        else {
            alertBox.showAlertWithBack("Unable to connect Internet. please check your Internet connection ! ");
        }
    }

    private void callApiForOEMoreDetails() {

        ProductSpeFragment specFragment = new ProductSpeFragment();

        ModelSuitsFragment modelSuitsFragment = new ModelSuitsFragment();
//        modelSuitsFragment.setSuitsData(response.body().getData());
        OtherResourcesFragment otherResourcesFragment = new OtherResourcesFragment();
//                                otherResourcesFragment.setResourceData(response.body().getData1(),response.body().getPdfModelsList());
//        otherResourcesFragment.setResourceData(videoList,pdfList);
        adapter.addFragment(modelSuitsFragment,"Suits these\nModel");
        adapter.addFragment(otherResourcesFragment,"Other\nResources");

//        adapter.addFragment(new ModelSuitsFragment(),"Videos");

//                                synchronized (object) {
//                                    if (!thread.isAlive())
//                                        specFragment.setImageList(productImageList);
//                                    else {
//                                        object.wait();
//        specFragment.setImageList(productImageList);
//                                    }

        pager.setAdapter(adapter);
        layouts.setupWithViewPager(pager);
    }
}
