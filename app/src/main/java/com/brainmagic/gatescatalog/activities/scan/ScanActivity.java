package com.brainmagic.gatescatalog.activities.scan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.imageupload.UploadImage;
import com.brainmagic.gatescatalog.api.models.scanresult.ScanResultReward;
import com.brainmagic.gatescatalog.api.models.uploaduserdetails.UploadUserdetails;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanActivity extends Activity {

    private RelativeLayout scan, enterpartnumber, scanhistory, clickpicture;
//    private Button clickpicture;
    private IntentIntegrator qrScan;
    private String p;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Email, Mobno, Usertype, Name, Bussinessname, longitude, latitude, address;
    ImageView selectimage;
    private ImagePicker imagePicker;
    private String mImageName = "";
    private File mImageFile;
    String imagename = "";
    private Uri i;
    ProgressDialog loading;
    ImageView menu;
    private FloatingActionButton home, back;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        qrScan = new IntentIntegrator(this);
        scan = findViewById(R.id.scan_layout);
        enterpartnumber = findViewById(R.id.enter_part_number);
        clickpicture = findViewById(R.id.click_picture);
        scanhistory = findViewById(R.id.scan_history);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Email = myshare.getString("Email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Name = myshare.getString("Name", "");
        Bussinessname = myshare.getString("Bname", "");
        address = getIntent().getStringExtra("location");
        latitude = getIntent().getStringExtra("latitude");
        longitude = getIntent().getStringExtra("longitude");
        if (Usertype.equals("Gates Employee")) {
            clickpicture.setVisibility(View.VISIBLE);
        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ScanActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrScan.setBeepEnabled(true);
                qrScan.setOrientationLocked(false);
                qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                qrScan.setCaptureActivity(AnyOrientationCaptureActivity.class);
                qrScan.initiateScan();
            }
        });

        enterpartnumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterprodcutserialalert();
            }
        });
        scanhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent scanhistory = new Intent(getApplicationContext(), ScanHistory.class);
                startActivity(scanhistory);
            }
        });
        clickpicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog uploadimage = new AlertDialog.Builder(ScanActivity.this).create();
                final View upimg = LayoutInflater.from(ScanActivity.this).inflate(R.layout.imageuploadalert, null);
                final Button uploadimagebt = upimg.findViewById(R.id.uploadimagebt);
                selectimage = upimg.findViewById(R.id.selectimage);

                selectimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imagePicker = new ImagePicker(ScanActivity.this, null, new OnImagePickedListener() {
                            @Override
                            public void onImagePicked(Uri imageUri) {
                                selectimage.setImageURI(imageUri);
                                mImageName = getFileName(imageUri);
                                mImageFile = getFileFromImage();
                                i = imageUri;
                                if (i.equals(null)) {

                                } else {
                                    uploadimagebt.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                        imagePicker.choosePicture(true);
                    }
                });

                uploadimagebt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        uploadimage(mImageFile);
                        uploadimage.dismiss();
                    }
                });
                uploadimage.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                uploadimage.setView(upimg);
                uploadimage.setCanceledOnTouchOutside(false);
                uploadimage.show();
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ScanActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(ScanActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(ScanActivity.this, About_Gate_Activity.class));
                                return true;

                            case R.id.pricelist:
                                startActivity(new Intent(ScanActivity.this, Price_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(ScanActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(ScanActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
//                                startActivity(new Intent(ScanActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(ScanActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(ScanActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(ScanActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(ScanActivity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(ScanActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });

    }

    public void NointernetNOBACK(String s) {

        Alertbox alert = new Alertbox(ScanActivity.this);
        alert.NointernetNOBACK(s);
    }

    private void enterprodcutserialalert() {
        final AlertDialog alertDialog = new AlertDialog.Builder(ScanActivity.this).create();
        final View enteralert = LayoutInflater.from(ScanActivity.this).inflate(R.layout.enterpartnumbervalidate, null);
        final EditText scannnumberenter = enteralert.findViewById(R.id.scannnumberenter);
        Button validateenter = enteralert.findViewById(R.id.validateenter);
        Button cancel = enteralert.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        validateenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String productnumb = scannnumberenter.getText().toString();
                alertDialog.dismiss();
                if (productnumb.equals("") || productnumb == null) {
                    Toast.makeText(ScanActivity.this, "Please Enter Product Serial Number", Toast.LENGTH_SHORT).show();
                } else if (productnumb.length() < 16) {
                    Toast.makeText(ScanActivity.this, "Please Enter valid 16 digit Product Serial Number", Toast.LENGTH_SHORT).show();
//                                                Toast.makeText(HomeScreenActivity.this, "Please Enter valid Product Serial", Toast.LENGTH_SHORT).show();
                } else {
                    scan(productnumb);
                }

            }
        });
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
        alertDialog.setView(enteralert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    public void Sucess(String s) {
        Alertbox alert = new Alertbox(ScanActivity.this);
        alert.Scansuccess(s);
    }

    public void Nointernet(String s) {
        Alertbox alert = new Alertbox(ScanActivity.this);
        alert.NointernetNOBACK(s);
    }

    private void scan(final String pnumber) {
        try {
            final ProgressDialog loading = ProgressDialog.show(ScanActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<ScanResultReward> call = service.scan(Mobno, pnumber, Usertype, Bussinessname, address, latitude, longitude);
            call.enqueue(new Callback<ScanResultReward>() {
                @Override
                public void onResponse(Call<ScanResultReward> call, Response<ScanResultReward> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("ScanSuccess")) {
//                            Sucess("Scanned & Verified Successfully, You will get Your Point Very Soon");
                            Sucess("Dear" + " " + myshare.getString("Name", "") + ", Scanned serial no " + pnumber + ". This number is eligible for reward points(यह नंबर रिवार्ड पॉइंट्स के लिए योग्य है). Thank You.");

                        } else if (response.body().getResult().equals("AlreadScaned")) {
//                            Nointernet("This Part Number is Already Scanned Not Eligible For Reward Points");
                            Nointernet("Dear" + " " + myshare.getString("Name", "") + ", Scanned serial no " + pnumber + ". This number is already scanned(यह संख्या पहले से ही स्कैन की गई है). You are not eligible for reward points. Thank You.");

                        } else {

//                            Nointernet("This Part Number is Not Eligible For Reward Points");
                            Nointernet("Dear" + " " + myshare.getString("Name", "") + ", Scanned serial no  " + pnumber + ". This number is not eligible for reward points(यह अंक रिवार्ड पॉइंट्स के लिए योग्य नहीं है). Thank You.");
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                        loading.dismiss();
                        Nointernet("This Part Number is Not Eligible For Reward Points");
                    }
                }

                @Override
                public void onFailure(Call<ScanResultReward> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Nointernet("This Part Number is Not Eligible For Reward Points");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Nointernet("Exception Occurred. Please try again Later");
        }

    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private File getFileFromImage() {
        try {
            BitmapDrawable drawable = (BitmapDrawable) selectimage.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            File directory = new File(getFilesDir(), "profile");
            if (!directory.exists())
                directory.mkdirs();
            File myappFile = new File(directory
                    + File.separator + mImageName);
            FileOutputStream fos = new FileOutputStream(myappFile);
            fos.write(byteArray);
//                        mImageName = File_URL + myappFile.getName();
            return myappFile;
        } catch (Exception e) {
            e.printStackTrace();
            return new File("");
        }
    }

    private void uploadimage(File selectimage) {
        try {
            loading = new ProgressDialog(ScanActivity.this);
            loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loading.setCancelable(false);
            loading.setMessage("Loading...");
            if (!loading.isShowing())
                loading.show();
            APIService service = RetrofitClient.getApiService();
            String type = "image/png";
            imagename = selectimage.getName();
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), selectimage);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", selectimage.getName(), requestBody);
            final Call<UploadImage> request = service.imageupload(filePart);
            request.enqueue(new Callback<UploadImage>() {
                @Override
                public void onResponse(Call<UploadImage> call, Response<UploadImage> response) {
                    if (response.body().getMessage().equals("Success.")) {
                        loading.dismiss();
                        uploaduserdetails();
                    }
//                                else {
//                                    Nointernet("Please Try Again later");
//                                }
                }

                @Override
                public void onFailure(Call<UploadImage> call, Throwable t) {
                    Log.v("Upload Exception", t.getMessage());
                    loading.dismiss();
                    t.printStackTrace();
                    Nointernet("Failed to Reach the Server. Please try again Later");

                }

            });
        } catch (Exception ex) {
            if (loading.isShowing())
                loading.dismiss();
            Log.v("Exception", ex.getMessage());
            Nointernet("Exception Occurred. Please try again Later");

        }
    }

    private void uploaduserdetails() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ScanActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<UploadUserdetails> call = service.gatesimageuserdetails(Mobno, imagename, address);
            call.enqueue(new Callback<UploadUserdetails>() {
                @Override
                public void onResponse(Call<UploadUserdetails> call, Response<UploadUserdetails> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            Sucess("Image Uploaded Successfully");

                        } else if (response.body().getResult().equals("NotSuccess")) {
                            Nointernet("Please Try Again later");

                        } else {
                            Nointernet("Please Try Again later");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        Nointernet("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<UploadUserdetails> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Nointernet("Failed to Reach the Server. Please try again Later");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Nointernet("Exception Occurred. Please try again Later");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                //Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                final AlertDialog scannednumber = new AlertDialog.Builder(ScanActivity.this).create();
                final View scan = LayoutInflater.from(ScanActivity.this).inflate(R.layout.scannednumber, null);
                final EditText scannnumber = scan.findViewById(R.id.scannnumberalert);
                Button validate = scan.findViewById(R.id.validatealert);
                scannnumber.setText(result.getContents());

                validate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String productnumb = scannnumber.getText().toString();
                        scannednumber.dismiss();
                        p = productnumb.replaceAll("\t", "");
                        scan(p);
                    }
                });

                scannednumber.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                scannednumber.setView(scan);
                scannednumber.setCanceledOnTouchOutside(false);
                scannednumber.show();
            }
        } else {
            if (requestCode == 200) {
                super.onActivityResult(requestCode, resultCode, data);
//            if (imagePicker.handleActivityResult(resultCode,requestCode,data!=null)) {
                if (resultCode == RESULT_OK) {
                    imagePicker.handleActivityResult(resultCode, requestCode, data);

                }
            }
        }
    }


}
