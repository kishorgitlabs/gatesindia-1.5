package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.brainmagic.gatescatalog.services.BackgroundService;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.Product_Grid_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeModel;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//import adapter.Passenger_Car_Adapter;

public class Product_Grid_Activity extends Activity {

    private ImageView menu;
    private TextView navicationtxt;
    private SQLiteDatabase db;
    private ListView listView;
    private Cursor c;
    private List<EngineCodeResult> data;
    private ArrayList<String> engine_codeList, model_codeList, year_fromList, year_tillList, month_fromList,
            month_tillList, stokeList, fromyearList, toyearList, partDescriptionList, gates_Part_List, equipment1List, equipment2List;
    private ArrayList<Integer> engineCodeList;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Email, Mobno, Usertype, Country, SelectedYearFrom = "All", SelectedYearTo = "All", vehicle_type, oem_name, vehicle_model;
    Product_Grid_Adapter adapter;
    private Spinner fromyearspinner, toyearspinner;
    //    private ImageView moreImage;
    private Alertbox box = new Alertbox(Product_Grid_Activity.this);

    private TextView ProfileName;
    //    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
    int x = 0, y = 0;
//    String yearFrom="",yearTill="";

    private String nameLog, mobileNoLog, userTypeLog, stateLog, cityLog, zipCodeLog, locationLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_grid);

        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        signout_relativelayout = findViewById(R.id.signout);
        signout_relativelayout.setVisibility(View.GONE);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);

        listView = (ListView) findViewById(R.id.listview);

        fromyearspinner = (Spinner) findViewById(R.id.fromyearspinner);
        toyearspinner = (Spinner) findViewById(R.id.toyearspinner);
        navicationtxt = (TextView) findViewById(R.id.navicationtxt);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);

        nameLog = myshare.getString("nameLogs", "");
        mobileNoLog = myshare.getString("MobNo", "");
        userTypeLog = myshare.getString("Usertype", "");
        stateLog = myshare.getString("state", "");
        cityLog = myshare.getString("city", "");
        zipCodeLog = myshare.getString("zipCode", "");
        locationLog = myshare.getString("streetName", "");

        editor = myshare.edit();

        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "") + " , " + Country);

        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(Product_Grid_Activity.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);
        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin", false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.grid_activity), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);

                    }
                });
                mySnackbar.show();
            }
        });

        final ImageView downArrow = includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData = includeviewLayout.findViewById(R.id.user_data);
        ViewGroup homeLayout = findViewById(R.id.grid_activity);

        downArrow.setOnClickListener(new View.OnClickListener() {
            boolean visible;

            @Override
            public void onClick(View v) {
                ChangeBounds changeBounds = new ChangeBounds();
                changeBounds.setDuration(600L);
                TransitionManager.beginDelayedTransition(includeviewLayout, changeBounds);
                visible = !visible;
                if (visible)
                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                userData.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

        engineCodeList = new ArrayList<Integer>();
        engine_codeList = new ArrayList<String>();
        model_codeList = new ArrayList<String>();
        year_fromList = new ArrayList<String>();

        fromyearList = new ArrayList<String>();
        toyearList = new ArrayList<String>();

        year_tillList = new ArrayList<String>();
        month_fromList = new ArrayList<String>();
        month_tillList = new ArrayList<String>();

        partDescriptionList = new ArrayList<String>();
        gates_Part_List = new ArrayList<String>();
        equipment1List = new ArrayList<String>();
        equipment2List = new ArrayList<String>();
        stokeList = new ArrayList<String>();


        vehicle_type = getIntent().getStringExtra("SEGMENT");
        oem_name = getIntent().getStringExtra("OEM");
        vehicle_model = getIntent().getStringExtra("MODEL");
        navicationtxt.setText(getIntent().getStringExtra("navitxt"));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent a = new Intent(Product_Grid_Activity.this, Product_Details_Activity.class);
//                a.putExtra("modelcode", data.get(position).getModelcode().toString());
//                a.putExtra("engine_codeList", data.get(position).getEnginecode().toString());
//                a.putExtra("model", vehicle_model);
//                a.putExtra("segment",vehicle_type);
//                a.putExtra("yearfrom",(data.get(position).getYearFrom()));
//                a.putExtra("yearto", (data.get(position).getYearTill()));
//                a.putExtra("engineCode", data.get(position).getEnginecode());
//                a.putExtra("modelCode",data.get(position).getModelcode());
//                a.putExtra("oem", oem_name);
//                a.putExtra("navtxt",  navicationtxt.getText().toString()+" > "+data.get(position).getEnginecode().toString().trim());
//                startActivity(a);

//                sendLog("EngineCode", data.get(position).getEnginecode());
                sendLog(vehicle_type, nameLog, vehicle_model, data.get(position).getArticle(),
                        mobileNoLog, userTypeLog, stateLog, cityLog, zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Grid_Activity.this, EngineSpecActivity.class);
                a.putExtra("modelcode", data.get(position).getModelcode().toString());
                a.putExtra("engine_codeList", data.get(position).getEnginecode().toString());
                a.putExtra("model", vehicle_model);
                a.putExtra("segment", vehicle_type);
                a.putExtra("yearfrom", (data.get(position).getYearFrom()));
                a.putExtra("yearto", (data.get(position).getYearTill()));
                 a.putExtra("engineCode", data.get(position).getEnginecode());
                a.putExtra("modelCode", data.get(position).getModelcode());
                a.putExtra("modelCode2WHCV", "NA");
                a.putExtra("oem", oem_name);
                a.putExtra("navtxt", navicationtxt.getText().toString() + " > " + data.get(position).getEnginecode().toString().trim());
                startActivity(a);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_Grid_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_Grid_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Product_Grid_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Product_Grid_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Product_Grid_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Product_Grid_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;

                            case R.id.pricelist:
                                Intent aw = new Intent(Product_Grid_Activity.this, OtherLinksActivity.class);
                                startActivity(aw);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_Grid_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Product_Grid_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Product_Grid_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Product_Grid_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Product_Grid_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_Grid_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Product_Grid_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Model_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();

            }
        });

//        GetYearFrom();
//        GetYearTo("");

        // String query ="select distinct Modelcode, Year_From,Month_From,Year_Till,Month_Till , Enginecode, Stroke  from product where   Model='" + vehicle_model + "' and Make='" + oem_name + "'  ";
        //new GetProductDetails().execute(query);
        fromyearspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                String yearFrom = fromyearspinner.getSelectedItem().toString();
                if (yearFrom.equals("All")) {
                    if (x != 0) {
                        if (!"All".equals(toyearspinner.getSelectedItem().toString()))
                            adapter.setListBasedYearTo(toyearspinner.getSelectedItem().toString());
                        else {
                            adapter.setOverAllList();
                        }
                    }
                    x++;
                } else {
                    adapter.setYearFromList(Integer.parseInt(yearFrom), toyearspinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        toyearspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                String yearFrom = fromyearspinner.getSelectedItem().toString();
                String yearTo = toyearspinner.getSelectedItem().toString();
                if (!TextUtils.isEmpty(yearTo) && yearTo.equals("All")) {
                    if (yearTo.equals("All")) {
                        if (y != 0)
                            if (!"All".equals(fromyearspinner.getSelectedItem().toString()))
                                adapter.setListBasedYearFrom(fromyearspinner.getSelectedItem().toString());
                            else adapter.setOverAllList();
                        y++;
                    }
                } else {
                    adapter.setYearToList(yearTo, fromyearspinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



   /*     toyearspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                SelectedYearTo = parent.getItemAtPosition(i).toString().trim();
                String query ="select distinct Modelcode, Year_From,Month_From,Year_Till,Month_Till,Enginecode, Stroke  from product where  Year_From >= "+SelectedYearFrom.trim()+" and Year_Till <="+SelectedYearTo.trim()+" and  Model='" + vehicle_model + "' and Make='" + oem_name + "' order by Modelcode asc ";
//                new GetProductDetails().execute(query);

                //Adapter Calls two times in onCreate so this logic is used to stop
                if(SelectedYearTo.equals("All")&&SelectedYearFrom.equals("All"))
                {
                    if(i!=0)
                    {
//                        new GetProductDetails().execute(query);
                    }
                    else {
                        i++;
                    }
                }

                else {
//                    new GetProductDetails().execute(query);
                }

               *//* if (!SelectedYearTo.equals("All"))
                {
                    if (!SelectedYearFrom.equals("All"))
                    {
                        String query ="select distinct Modelcode, Year_From,Month_From,Year_Till,Month_Till , Enginecode, Stroke  from product where  Year_From >= "+SelectedYearFrom.trim()+" and Year_Till <="+SelectedYearTo.trim()+" and  Model='" + vehicle_model + "' and Make='" + oem_name + "' order by Modelcode asc ";
                        new GetProductDetails().execute(query);
                    }
                    else
                    {
                        String query ="select distinct Modelcode, Year_From,Month_From,Year_Till,Month_Till , Enginecode, Stroke  from product where  Year_Till <= "+SelectedYearTo.trim()+" and   Model='" + vehicle_model + "' and Make='" + oem_name + "' order by Modelcode asc ";
                        new GetProductDetails().execute(query);
                    }

                }
                else
                {
                    if (!SelectedYearFrom.equals("All"))
                    {
                        String query ="select distinct Modelcode, Year_From,Month_From,Year_Till,Month_Till , Enginecode, Stroke  from product where   Year_From >="+SelectedYearFrom.trim()+" and  Model='" + vehicle_model + "' and Make='" + oem_name + "' order by Modelcode asc ";
                        new GetProductDetails().execute(query);
                    }
                    else
                    {
                        String query ="select distinct Modelcode, Year_From,Month_From,Year_Till,Month_Till , Enginecode, Stroke  from product where  Model='" + vehicle_model + "' and Make='" + oem_name + "' order by Modelcode asc ";
                        new GetProductDetails().execute(query);
                    }
                }*//*


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

//        GetYearFrom();
//        GetYearTo("");
//        setListView();
        checkInternet();
    }

    private void checkInternet() {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Grid_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            getEngineList();
        } else {
            box.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }


    /*private void sendLog(String productName, String productDetail) {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Grid_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            startService(new Intent(Product_Grid_Activity.this, BackgroundService.class)
                    .putExtra("mobileNo", myshare.getString("MobNo", "000"))
                    .putExtra("productName", productName)
                    .putExtra("productDetail", productDetail)
            );
        }
    }*/

    private void sendLog(String segment, String userName, String vehicleName, String partNo, String mobileNumber, String userTypes, String stateLog,
                         String CityLog, String ZipCodeLog, String location, String deviceType)
    {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Grid_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            startService(new Intent(Product_Grid_Activity.this, BackgroundService.class)
                    .putExtra("segmentLog", segment)
                    .putExtra("nameLog",userName)
                    .putExtra("vehicleNameLog",vehicleName)
                    .putExtra("partNoLog", partNo)
                    .putExtra("mobileNoLog",mobileNumber)
                    .putExtra("userTypeLog",userTypes)
                    .putExtra("stateLog", stateLog)
                    .putExtra("cityLog",CityLog)
                    .putExtra("zipCodeLog",ZipCodeLog)
                    .putExtra("locationLog", location)
                    .putExtra("deviceTypeLog",deviceType)
            );
        }
    }

    private void getEngineList() {
        final CustomProgressDialog progressDialog = new CustomProgressDialog(Product_Grid_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();

            Call<EngineCodeModel> call = service.getEngineList(vehicle_type, oem_name, vehicle_model);

            call.enqueue(new Callback<EngineCodeModel>() {
                @Override
                public void onResponse(Call<EngineCodeModel> call, Response<EngineCodeModel> response) {
                    progressDialog.dismiss();
                    try {

                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                List<String> yearFromList = response.body().getmData1().getYearFrom();
                                yearFromList.add(0, "All");
                                List<String> yearToList = response.body().getmData1().getYearTill();
                                yearToList.add(0, "All");
                                ArrayAdapter<String> fromYearAdapter = new ArrayAdapter(Product_Grid_Activity.this, R.layout.simple_spinner_item, yearFromList);
                                fromyearspinner.setAdapter(fromYearAdapter);
                                ArrayAdapter<String> toYearAdapter = new ArrayAdapter(Product_Grid_Activity.this, R.layout.simple_spinner_item, yearToList);
                                toyearspinner.setAdapter(toYearAdapter);

//                                model.clear();
//                                model.addAll(response.body().getData());
                                data = response.body().getData();
                                adapter = new Product_Grid_Adapter(Product_Grid_Activity.this, response.body().getData());
                                listView.setAdapter(adapter);


                            } else {
                                box.showAlertWithBack("No Record Found");
                            }
                        } else {
                            box.showAlertWithBack("Could not Connect to Server. Please try again Later");
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        box.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }


                @Override
                public void onFailure(Call<EngineCodeModel> call, Throwable t) {
                    progressDialog.dismiss();
                    box.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            box.showAlertWithBack("Exception Occurred. Please try again Later");
        }

    }

    private void GetYearFrom() {

//        SQLiteHelper dbHelper = new SQLiteHelper(Product_Grid_Activity.this);
//        db = dbHelper.getReadableDatabase();
        fromyearList.clear();
        fromyearList.add("All");

        String selectQuery = "select distinct Year_From from product where  Model='" + vehicle_model + "' and Make='" + oem_name + "' ORDER BY Year_From asc";

        c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                if (!(c.getString(c.getColumnIndex("Year_From")).equals("")))
                    fromyearList.add(c.getString(c.getColumnIndex("Year_From")).trim());
            }
            while (c.moveToNext());

        }
//        ArrayAdapter<String> country_adapter = new ArrayAdapter<String>(Product_Grid_Activity.this, R.layout.simple_spinner_item, fromyearList);
//        fromyearspinner.setAdapter(country_adapter);
//        c.close();
//        db.close();

    }

    private void GetYearTo(String SelectedYearFrom) {

//        SQLiteHelper dbHelper = new SQLiteHelper(Product_Grid_Activity.this);
//        db = dbHelper.getReadableDatabase();
        toyearList.clear();
        toyearList.add("All");
        String selectQuery;
        if (SelectedYearFrom.equals("")) {
            selectQuery = "select distinct Year_Till from product where Model='" + vehicle_model + "' and  Make='" + oem_name + "' ORDER BY Year_Till asc";
        } else {
            selectQuery = "select distinct Year_Till from product where Model='" + vehicle_model + "' and Year_Till > " + SelectedYearFrom.trim() + " and Make='" + oem_name + "' ORDER BY Year_Till asc";
        }

        c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                if (!(c.getString(c.getColumnIndex("Year_Till")).equals("")))
                    toyearList.add(c.getString(c.getColumnIndex("Year_Till")).trim());
            }
            while (c.moveToNext());

        }
//        ArrayAdapter<String> country_adapter = new ArrayAdapter<String>(Product_Grid_Activity.this, R.layout.simple_spinner_item, toyearList);
//        toyearspinner.setAdapter(country_adapter);
        c.close();
        db.close();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);
    }


}


