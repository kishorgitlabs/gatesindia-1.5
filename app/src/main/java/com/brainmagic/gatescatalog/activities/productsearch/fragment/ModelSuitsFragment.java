package com.brainmagic.gatescatalog.activities.productsearch.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.brainmagic.gatescatalog.adapter.ModelSuitAdapter;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.adapter.threeepandablelist.ParentMakeExAdapter;
import com.brainmagic.gatescatalog.api.models.moredetails.ModelDetailsResult;

import java.util.List;

public class ModelSuitsFragment extends Fragment {

    ExpandableListView expandableListView;
    private List<ModelDetailsResult> data;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_videos, container, false);
        ListView makeLists = view.findViewById(R.id.make_lists);



        makeLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });


        try {

            expandableListView = view.findViewById(R.id.expandableListView_Parent);
            LinearLayout errorLayout = view.findViewById(R.id.error_layout);

            if (data != null) {
                if (data.size() != 0) {
                    ParentMakeExAdapter exAdapter = new ParentMakeExAdapter(getActivity(), data);
                    expandableListView.setAdapter(exAdapter);
//                    ModelSuitAdapter suitAdapter = new ModelSuitAdapter(getActivity(), data);
//                    makeLists.setAdapter(suitAdapter);
                } else {
                    expandableListView.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                }
            } else {
                expandableListView.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "startpage: "+e.toString(), Toast.LENGTH_SHORT).show();
        }


        return view;
    }

    public void setSuitsData(List<ModelDetailsResult> data) {
        this.data = data;
    }


}
