package com.brainmagic.gatescatalog.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.PriceListAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.PriceList.PricePojo;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Price_Activity extends AppCompatActivity {


    private ImageView menu;
    private EditText pricelist_partno;
    private FloatingActionButton home,back;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ListView price_listview;
    private Alertbox alertBox;
   private MaterialSpinner pricelist_spinner;
   private Button pricelist_search;
   private LinearLayout part_no_enter_layout,part_no_search_layout,topic_layout,partslistview_layout;
   private String getpartnumbers,getall;
   private PriceListAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_);

        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);
        pricelist_partno=findViewById(R.id.pricelist_partno);
        price_listview=findViewById(R.id.price_listview);
        pricelist_spinner=findViewById(R.id.pricelist_spinner);
        pricelist_search=findViewById(R.id.pricelist_search);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        alertBox = new Alertbox(Price_Activity.this);

        part_no_enter_layout=findViewById(R.id.part_no_enter_layout);
        part_no_search_layout=findViewById(R.id.part_no_search_layout);
        topic_layout=findViewById(R.id.topic_layout);
        partslistview_layout=findViewById(R.id.partslistview_layout);




        pricelist_spinner.setItems("All","Gates Part No","OEM Part No");

        pricelist_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                click();


            }
        });
        pricelist_spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                String parts=item.toString();



                if (parts.equals("All")){
                    part_no_enter_layout.setVisibility(View.INVISIBLE);
                    topic_layout.setVisibility(View.INVISIBLE);
                    partslistview_layout.setVisibility(View.INVISIBLE);
                }
                else {

                    part_no_enter_layout.setVisibility(View.VISIBLE);
                    part_no_search_layout.setVisibility(View.VISIBLE);
                    topic_layout.setVisibility(View.INVISIBLE);
                    partslistview_layout.setVisibility(View.INVISIBLE);
                    partclicks();
                }


            }
        });




        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Price_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
//
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });




        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Price_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Price_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Price_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Price_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;

                            case R.id.scan:
                                startActivity(new Intent(Price_Activity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Price_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Price_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Price_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Price_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Price_Activity.this, Distributor_Search_Activity.class));
                                return true;

                            case R.id.contactpop:
                                startActivity(new Intent(Price_Activity.this, Contact_Details_Activity.class));
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });


    }

private void partclicks(){


        pricelist_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

getpartnumbers=pricelist_partno.getText().toString();

if (getpartnumbers.equals("")){

    alertBox.showAlert(getString(R.string.alerts));
}
else{
    checkinternet();
}

            }
        });


}


    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(Price_Activity.this);

        if (connection.CheckInternet()) {
            getMoreDetails();
        } else {
            alertBox.showAlertWithBack(getString(R.string.internet_error_msg));
        }


    }
    private void click(){
        update();

    }

    private void update(){
        Network_Connection_Activity connection = new Network_Connection_Activity(Price_Activity.this);

        if (connection.CheckInternet()) {
            getalldetails();
        } else {
            alertBox.showAlertWithBack(getString(R.string.internet_error_msg));
        }


    }

    private void getalldetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Price_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetrofitClient.getApiService();

        try {
            Call<PricePojo> call = service.prices("");
            call.enqueue(new Callback<PricePojo>() {
                @Override
                public void onResponse(Call<PricePojo> call, Response<PricePojo> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")) {

                         topic_layout.setVisibility(View.VISIBLE);
                                partslistview_layout.setVisibility(View.VISIBLE);
                                price_listview.setVisibility(View.VISIBLE);
                                adapter=new PriceListAdapter(Price_Activity.this,response.body().getData());
                                price_listview.setAdapter(adapter);
                            }
                            else {
                                alertBox.showAlertWithBack("No Record Found");
                            }
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<PricePojo> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

    private void getMoreDetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Price_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetrofitClient.getApiService();

        try {
            Call<PricePojo> call = service.prices(getpartnumbers);
            call.enqueue(new Callback<PricePojo>() {
                @Override
                public void onResponse(Call<PricePojo> call, Response<PricePojo> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")) {


                                topic_layout.setVisibility(View.VISIBLE);
                                partslistview_layout.setVisibility(View.VISIBLE);
                                price_listview.setVisibility(View.VISIBLE);
                                adapter=new PriceListAdapter(Price_Activity.this,response.body().getData());
                                price_listview.setAdapter(adapter);
                            }
                            else {
                                alertBox.showAlertWithBack("No Record Found");
                            }
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<PricePojo> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }
}
