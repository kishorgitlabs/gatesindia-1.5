package com.brainmagic.gatescatalog.activities.productsearch.whatsnew;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.adapter.ImageAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.font.CustomFont;
import com.brainmagic.gatescatalog.font.FontDesign;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.File;

import uk.co.senab.photoview.PhotoView;


public class Whats_New_More_Details extends Activity
        implements ImageAdapter.GetImage
{
    private ImageView product_image;
    private String vehicle_type, engine_code, year_from, year_to, month_from, month_to, stroke;
    private String oem_name, model_code, model_name, part_no;
    String Email, Mobno, Usertype,Country,ProductURL;
    ImageView menu;
    private FontDesign list_modelcode,  list_model, list_monthfrom, list_monthtill, list_partdescription, list_oem,
            list_gatespartno, list_engine_code,list_equipment1, list_equipment2, list_equipment_from_date, list_vehicleType, list_stroke,
            list_equipment_to_date,list_ProductAdditionalInfo,list_appAtt,list_cc,list_kw;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox box = new Alertbox(Whats_New_More_Details.this);
    private RelativeLayout imageView;
    private TextView ProfileName;
//    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private View includeviewLayout;
    private PopupWindow mPopupWindow;
    private RelativeLayout mRelativeLayout;
    private String fullImage;
    private ImageView profilePicture;
    private String MonthYearFrom,MonthYearTill;
    private CustomFont navigationText;
    ImageView rifhtbtn,leftbtn;
//    private SwipeRefreshLayout swipeRefreshLayout;
    private PhotoView photoView;
    private View customView;
    private Integer imgUrl;
    private String listmonthfrom,listmonthtill,listyearfrom,listyeartill,yearfromstng,yeartostng;
//    private List<LogDetailResult> thumbImage;
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_more_details);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);


        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        RelativeLayout supportCountryLayout=findViewById(R.id.supported_country_layout);
        TextView supportCountryList=findViewById(R.id.list_supportcountry);
        includeviewLayout = findViewById(R.id.profile_logo);
        navigationText = findViewById(R.id.navicationtxt);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
        signout_relativelayout =  findViewById(R.id.signout);
        signout_relativelayout.setVisibility(View.GONE);
        customView = inflater.inflate(R.layout.imagepopup, null);
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
//        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe);
        product_image = (ImageView) findViewById(R.id.product_image);
        imageView = (RelativeLayout)findViewById(R.id.tab5);
        list_oem =  findViewById(R.id.list_oem);
        list_vehicleType =  findViewById(R.id.list_vehicleType);
        list_modelcode =  findViewById(R.id.list_model_code);
        list_model =  findViewById(R.id.list_model);
//        list_stroke =  findViewById(R.id.list_stroke);

        rifhtbtn =  findViewById(R.id.right);
        leftbtn =  findViewById(R.id.left);
        list_monthfrom =  findViewById(R.id.list_monthfrom);

        list_monthtill =  findViewById(R.id.list_monthtill);
        list_partdescription =  findViewById(R.id.list_partdescription);
        list_gatespartno =  findViewById(R.id.list_gatespartno);


        list_appAtt =  findViewById(R.id.app_att);
        list_stroke =  findViewById(R.id.stroke);
        list_engine_code =  findViewById(R.id.list_engine_code);


        list_cc =  findViewById(R.id.cc_value);
        list_kw =  findViewById(R.id.kw_value);

//        list_equipment1 = (TextView) findViewById(R.id.list_equipment1);
//        list_equipment2 = (TextView) findViewById(R.id.list_equipment2);
//
//        list_equipment_from_date = (TextView) findViewById(R.id.list_equipment_fdate);
//        list_equipment_to_date = (TextView) findViewById(R.id.list_equipment_tdate);

        list_engine_code =  findViewById(R.id.list_engine_code);
        list_ProductAdditionalInfo =  findViewById(R.id.list_addtional);

//        mRelativeLayout = (RelativeLayout)findViewById(R.id.Whats_New_More_Details);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Intent i =getIntent();
        imgUrl = i.getIntExtra("URL",0);
        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");

        navigationText.setText(getIntent().getStringExtra("naviText"));
        ProfileName.setText(myshare.getString("name", "")+" , "+Country);
//        String SupportCountries=i.getStringExtra("supportedCountryList");

//        if(Country.equals("All Countries"))
//        {
//            supportCountryLayout.setVisibility(View.VISIBLE);
//            supportCountryList.setText(SupportCountries);
//        }


        if (!myshare.getString("profile_path", "").equals(""))
        Glide.with(Whats_New_More_Details.this)
                .load(new File(myshare.getString("profile_path", "")))
                .into(profilePicture);

        if(myshare.getBoolean("islogin",false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= 21) {

            mPopupWindow.setElevation(5.0f);

        }

        //To refresh the main image, user can swipe down
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
////                product_image.setImageResource(imgUrl);
//                finish();
//                startActivity(getIntent());
//                //  onRefresh();
//                //Picasso.with(Product_More_Details_Activity.this).load(imgUrl).error(R.drawable.noimages).into(product_image);
////                recreate();
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        swipeRefreshLayout.setRefreshing(false);
//                    }
//                },4000);
//            }
//        });
        product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
//                photoView = (PhotoView) customView.findViewById(R.id.pop_photo);
//                Picasso.with(Whats_New_More_Details.this).load(fullImage).error(R.drawable.noimages).into(photoView);
            }
        });

        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();
            }
        });
        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin",false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.imagecapture), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                mySnackbar.show();

            }
        });

        ImageView downArrow=includeviewLayout.findViewById(R.id.down_arrow);
        RelativeLayout userData=includeviewLayout.findViewById(R.id.user_data);
//        ViewGroup homeLayout=findViewById(R.id.Whats_New_More_Details);

//        downArrow.setOnClickListener(new View.OnClickListener() {
//            boolean visible;
//            @Override
//            public void onClick(View v) {
//                TransitionManager.beginDelayedTransition(homeLayout);
//                visible = !visible;
//                if(visible)
//                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
//                else
//                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
//                userData.setVisibility(visible? View.VISIBLE: View.GONE);
//            }
//        });

        oem_name = getIntent().getStringExtra("OEM");
        model_code = getIntent().getStringExtra("MODEL_CODE");
        model_name = getIntent().getStringExtra("MODEL");
        part_no = getIntent().getStringExtra("GATESPART");

        listmonthfrom = getIntent().getStringExtra("MONTHFROM");
        listmonthtill =  getIntent().getStringExtra("MONTHTILL");
        listyearfrom =getIntent().getStringExtra("YEARFROM");
        listyeartill = getIntent().getStringExtra("YEARTILL");


        oem_name = getIntent().getStringExtra("OEM");
        model_code = getIntent().getStringExtra("MODEL_CODE");
        model_name = getIntent().getStringExtra("MODEL");
        part_no = getIntent().getStringExtra("GATESPART");
        engine_code = getIntent().getStringExtra("ENGINE_CODE");
//        internetcheck = getIntent().getStringExtra("internet");
//        thumbImage = (List<LogDetailResult>) getIntent().getSerializableExtra("thumbuimage");
        list_oem.setText(getIntent().getStringExtra("OEM"));
        list_modelcode.setText(getIntent().getStringExtra("MODEL_CODE"));
        list_model.setText(getIntent().getStringExtra("MODEL"));
        list_partdescription.setText(getIntent().getStringExtra("PARTDESC"));
        list_gatespartno.setText(getIntent().getStringExtra("GATESPART"));
        list_appAtt.setText(getIntent().getStringExtra("APPATT"));
        list_stroke.setText(getIntent().getStringExtra("STROKE"));
        list_cc.setText(getIntent().getStringExtra("CC"));
        list_kw.setText(getIntent().getStringExtra("KW"));
        //  list_imagename =getIntent().getStringExtra("IMAGENAME");
        yearfromstng = getIntent().getStringExtra("yearfrom");
        yeartostng = getIntent().getStringExtra("yearto");

//        thumbImage = (List<LogDetailResult>) getIntent().getSerializableExtra("thumbuimage");
//        if(thumbImage==null){
//            imageView.setVisibility(View.GONE);
//        }else if(thumbImage.size() <4) {
//            rifhtbtn.setVisibility(View.INVISIBLE);
//            leftbtn.setVisibility(View.INVISIBLE);
//            if(thumbImage.size()==1)
//                imageView.setVisibility(View.GONE);
//            else
//                imageView.setVisibility(View.VISIBLE);
//        }else {
//            imageView.setVisibility(View.VISIBLE);
//        }
        if(!TextUtils.isEmpty(listmonthfrom) && !TextUtils.isEmpty(listyearfrom))
            MonthYearFrom = listmonthfrom + "/" + listyearfrom;
        else if(!TextUtils.isEmpty(listmonthfrom) && TextUtils.isEmpty(listyearfrom))
            MonthYearFrom = listmonthfrom +"/"+ "";
        else if(TextUtils.isEmpty(listmonthfrom) && !TextUtils.isEmpty(listyearfrom))
            MonthYearFrom = "" +"/"+listyearfrom;
        else
            MonthYearFrom = "";


        if(!TextUtils.isEmpty(listmonthtill) && !TextUtils.isEmpty(listyeartill))
            MonthYearTill = listmonthtill + "/" + listyeartill;
        else if(!TextUtils.isEmpty(listmonthtill) && TextUtils.isEmpty(listyeartill))
            MonthYearTill = listmonthtill +"/"+ "";
        else if(TextUtils.isEmpty(listmonthtill) && !TextUtils.isEmpty(listyeartill))
            MonthYearTill = "" +"/"+listyeartill;
        else
            MonthYearTill = "";

        list_oem.setText("" + getIntent().getStringExtra("OEM"));
        list_modelcode.setText("" + getIntent().getStringExtra("MODEL_CODE"));
        list_model.setText("" + getIntent().getStringExtra("MODEL"));
        list_partdescription.setText("" + getIntent().getStringExtra("PARTDESC"));
        list_gatespartno.setText("" + getIntent().getStringExtra("GATESPART"));
//        list_equipment1.setText("" + getIntent().getStringExtra("EQUIPMENT1"));
//        list_equipment2.setText("" + getIntent().getStringExtra("EQUIPMENT2"));
//        list_equipment_from_date.setText("" + getIntent().getStringExtra("EQUIPMENTDATEF"));
//        list_equipment_to_date.setText("" + getIntent().getStringExtra("EQUIPMENTDATEFT"));

//        list_stroke.setText("" + getIntent().getStringExtra("STROKE"));
        list_monthfrom.setText(MonthYearFrom);
        list_monthtill.setText(MonthYearTill);
        list_vehicleType.setText("" + getIntent().getStringExtra("SEGMENT"));
        list_engine_code.setText("" + getIntent().getStringExtra("ENGINE_CODE"));
        list_ProductAdditionalInfo.setText("" + getIntent().getStringExtra("ProductAdditionalInfo"));

//        ProductURL =getIntent().getStringExtra("ProductURL").replaceAll(" ","%20");


        Log.v("image name", list_partdescription.getText() + ".jpg");

       /* if (list_partdescription.equals("")) {
            Picasso.with(Whats_New_More_Details.this).load("file:///android_asset/noimagefound.jpg").into(product_image);
        } else {
            Picasso.with(Whats_New_More_Details.this).load(myshare.getString("")).into(product_image);
        }*/
        if(!((ProductURL==null) || ProductURL.equals(""))) {
            fullImage=ProductURL;
            Picasso.with(Whats_New_More_Details.this).load(fullImage).error(R.drawable.noimages).into(product_image);
        }
        else Picasso.with(Whats_New_More_Details.this).load(R.drawable.noimages).into(product_image);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Whats_New_More_Details.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Whats_New_More_Details.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Whats_New_More_Details.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Whats_New_More_Details.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Whats_New_More_Details.this, Price_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Whats_New_More_Details.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Whats_New_More_Details.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Whats_New_More_Details.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Whats_New_More_Details.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Whats_New_More_Details.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Whats_New_More_Details.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Whats_New_More_Details.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Whats_New_More_Details.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Whats_New_More_Details.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Model_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });
        initialRecycler();


    }


    private void initialRecycler() {


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = findViewById(R.id.recycleview1);


        recyclerView.setLayoutManager(linearLayoutManager);
//        ImageAdapter imageAdapter = new ImageAdapter(this, productImageList);
//        recyclerView.setAdapter(imageAdapter);

        rifhtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*recyclerView.setLayoutManager(linearLayoutManager);
                linearLayoutManager.scrollToPosition(mImage.size()+1);*/
                recyclerView.scrollBy(100,0);
            }
        });

        leftbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerView.smoothScrollToPosition(0);
            }
        });

//        product_image.setImageResource(imgUrl);
    }

    public File getImage(String imagename) {

        File mediaImage = null;
        Bitmap b = null ;
        try {
            // String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(myshare.getString("ImagePath", ""));
            if (!myDir.exists())
                return null;

            mediaImage = new File(myDir.getPath()+"/"+imagename+".jpg");
            if (mediaImage.getAbsolutePath() != null)
                b = BitmapFactory.decodeFile(mediaImage.getAbsolutePath());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mediaImage;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);


    }

    @Override
    public void setOnImageClick(String image, int position) {
        fullImage=image;
        Picasso.with(Whats_New_More_Details.this).load(image).error(R.drawable.noimages).into(product_image);
//        product_image.setImageResource(p);
//        imageAdapter.notifyDataSetChanged();
    }
}
