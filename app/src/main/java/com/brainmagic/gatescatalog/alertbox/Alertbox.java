package com.brainmagic.gatescatalog.alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gates.R;


public class Alertbox {
    Context context;

    public Alertbox(Context con) {
        // TODO Auto-generated constructor stub
        this.context = con;
    }


    public void showAlert(String msg) {
        final AlertDialog alertDialog = new Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);
        ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText(msg);
        sucess.setImageResource(R.drawable.nointernet);
        okay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();

            }
        });
        Window window = alertDialog.getWindow();
        window.setLayout(600,600);
        alertDialog.show();
    }

    public void showAlertWithBack(String msg) {
        final AlertDialog alertDialog = new Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);
        ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText(msg);
        sucess.setImageResource(R.drawable.checked);
        okay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                ((Activity) context).onBackPressed();
            }
        });
        alertDialog.show();
    }

    public void alerte(String msg) {
        final AlertDialog alertDialog = new Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);
        ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText(msg);
        sucess.setImageResource(R.drawable.checked);
        okay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }


    public void Scansuccess(String msg) {
        final AlertDialog alertDialog = new Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);
        ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText(msg);
        sucess.setImageResource(R.drawable.checked);

        okay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).onBackPressed();
            }
        });
        alertDialog.show();
    }


    public void NointernetNOBACK(String msg) {
        final AlertDialog alertDialog = new Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);
        ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText(msg);
        sucess.setImageResource(R.drawable.nointernet);
        okay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                //((Activity) context).onBackPressed();
            }
        });
        Window window = alertDialog.getWindow();
        window.setLayout(600,600);
        alertDialog.show();
    }
}
