package com.brainmagic.gatescatalog.networkconnection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by BRAINMAGIC on 22-07-2016.
 */
public class Network_Connection_Activity {

    Context context;
    boolean checkingInternet;
    public Network_Connection_Activity(Context context) {
        this.checkingInternet = Boolean.valueOf(false);
        this.context = context;
    }


    public boolean CheckInternet() {
        ConnectivityManager connec = (ConnectivityManager) this.context.getSystemService("connectivity");
        NetworkInfo wifi = connec.getNetworkInfo(1);
        NetworkInfo mobile = connec.getNetworkInfo(0);
        if (wifi.isConnected()) {
            this.checkingInternet = Boolean.valueOf(true);
        } else if (mobile.isConnected()) {
            this.checkingInternet = Boolean.valueOf(true);
        } else {
            this.checkingInternet = Boolean.valueOf(false);
        }
        return this.checkingInternet;
    }


}

