package com.brainmagic.gatescatalog.api.models;

public class GetMechaniScores{
	private String result;
	private int data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(int data){
		this.data = data;
	}

	public int getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"GetMechaniScores{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
