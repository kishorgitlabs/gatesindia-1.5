package com.brainmagic.gatescatalog.api.models.PriceList;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OePartListPojo {

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<OePartList> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<OePartList> data){
		this.data = data;
	}

	public List<OePartList> getData(){
		return data;
	}
}