package com.brainmagic.gatescatalog.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PointsDetailsPojo {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private GetPointHistory data = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public GetPointHistory getData() {
        return data;
    }

    public void setData(GetPointHistory data) {
        this.data = data;
    }

}
