package com.brainmagic.gatescatalog.api.models.whatsnew;

import com.brainmagic.gatescatalog.api.models.QuizGetClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuizPojo {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private List<QuizGetClass> data = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<QuizGetClass> getData() {
        return data;
    }

    public void setData(List<QuizGetClass> data) {
        this.data = data;
    }
}
