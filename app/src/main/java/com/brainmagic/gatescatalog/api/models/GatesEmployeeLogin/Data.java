package com.brainmagic.gatescatalog.api.models.GatesEmployeeLogin;

public class Data{
	private String approveStatus;
	private String address;
	private Object latitude;
	private String mobileno;
	private Object gatesEmpName;
	private String gstin;
	private String registrationType;
	private String zipcode;
	private int id;
	private String email;
	private String password;
	private String status;
	private String businessName;
	private String insertdate;
	private String usertype;
	private String city;
	private Object longitude;
	private String date;
	private String shopname;
	private Object welcomeBonus;
	private String state;
	private String appid;
	private String name;
	private String country;
	private String oTPStatus;
	private Object updatetime;
	private Object username;
	private Object location;

	public void setApproveStatus(String approveStatus){
		this.approveStatus = approveStatus;
	}

	public String getApproveStatus(){
		return approveStatus;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLatitude(Object latitude){
		this.latitude = latitude;
	}

	public Object getLatitude(){
		return latitude;
	}

	public void setMobileno(String mobileno){
		this.mobileno = mobileno;
	}

	public String getMobileno(){
		return mobileno;
	}

	public void setGatesEmpName(Object gatesEmpName){
		this.gatesEmpName = gatesEmpName;
	}

	public Object getGatesEmpName(){
		return gatesEmpName;
	}

	public void setGstin(String gstin){
		this.gstin = gstin;
	}

	public String getGstin(){
		return gstin;
	}

	public void setRegistrationType(String registrationType){
		this.registrationType = registrationType;
	}

	public String getRegistrationType(){
		return registrationType;
	}

	public void setZipcode(String zipcode){
		this.zipcode = zipcode;
	}

	public String getZipcode(){
		return zipcode;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setBusinessName(String businessName){
		this.businessName = businessName;
	}

	public String getBusinessName(){
		return businessName;
	}

	public void setInsertdate(String insertdate){
		this.insertdate = insertdate;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public void setUsertype(String usertype){
		this.usertype = usertype;
	}

	public String getUsertype(){
		return usertype;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setLongitude(Object longitude){
		this.longitude = longitude;
	}

	public Object getLongitude(){
		return longitude;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setShopname(String shopname){
		this.shopname = shopname;
	}

	public String getShopname(){
		return shopname;
	}

	public void setWelcomeBonus(Object welcomeBonus){
		this.welcomeBonus = welcomeBonus;
	}

	public Object getWelcomeBonus(){
		return welcomeBonus;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setAppid(String appid){
		this.appid = appid;
	}

	public String getAppid(){
		return appid;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setOTPStatus(String oTPStatus){
		this.oTPStatus = oTPStatus;
	}

	public String getOTPStatus(){
		return oTPStatus;
	}

	public void setUpdatetime(Object updatetime){
		this.updatetime = updatetime;
	}

	public Object getUpdatetime(){
		return updatetime;
	}

	public void setUsername(Object username){
		this.username = username;
	}

	public Object getUsername(){
		return username;
	}

	public void setLocation(Object location){
		this.location = location;
	}

	public Object getLocation(){
		return location;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"approveStatus = '" + approveStatus + '\'' + 
			",address = '" + address + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",mobileno = '" + mobileno + '\'' + 
			",gates_emp_name = '" + gatesEmpName + '\'' + 
			",gstin = '" + gstin + '\'' + 
			",registrationType = '" + registrationType + '\'' + 
			",zipcode = '" + zipcode + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",password = '" + password + '\'' + 
			",status = '" + status + '\'' + 
			",businessName = '" + businessName + '\'' + 
			",insertdate = '" + insertdate + '\'' + 
			",usertype = '" + usertype + '\'' + 
			",city = '" + city + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",date = '" + date + '\'' + 
			",shopname = '" + shopname + '\'' + 
			",welcome_bonus = '" + welcomeBonus + '\'' + 
			",state = '" + state + '\'' + 
			",appid = '" + appid + '\'' + 
			",name = '" + name + '\'' + 
			",country = '" + country + '\'' + 
			",oTPStatus = '" + oTPStatus + '\'' + 
			",updatetime = '" + updatetime + '\'' + 
			",username = '" + username + '\'' + 
			",location = '" + location + '\'' + 
			"}";
		}
}
