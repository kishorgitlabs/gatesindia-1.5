package com.brainmagic.gatescatalog.api.models;

public class QuizModelClass {

    String Question_no;
    String Question_E;
    String Correct_answer_E;

    public String getQuestion_no() {
        return Question_no;
    }

    public void setQuestion_no(String question_no) {
        Question_no = question_no;
    }

    public String getQuestion_E() {
        return Question_E;
    }

    public void setQuestion_E(String question_E) {
        Question_E = question_E;
    }

    public String getCorrect_answer_E() {
        return Correct_answer_E;
    }

    public void setCorrect_answer_E(String correct_answer_E) {
        Correct_answer_E = correct_answer_E;
    }
}
