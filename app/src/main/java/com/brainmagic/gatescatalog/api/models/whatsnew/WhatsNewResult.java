
package com.brainmagic.gatescatalog.api.models.whatsnew;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class WhatsNewResult {

    @SerializedName("ActiveMessage")
    private String mActiveMessage;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("date")
    private String mDate;
    @SerializedName("deletestatus")
    private String mDeletestatus;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("EngineSpecification")
    private String mEngineSpecification;
    @SerializedName("gatpartno")
    private String mGatpartno;
    @SerializedName("Model")
    private String mModel;
    @SerializedName("Mrp")
    private String mMrp;
    @SerializedName("OEPartNo")
    private String mOEPartNo;
    @SerializedName("ProductAttachment")
    private String mProductAttachment;
    @SerializedName("Producttype")
    private String mProducttype;
    @SerializedName("Segement")
    private String mSegement;
    @SerializedName("Vehicle")
    private String mVehicle;
    @SerializedName("WhatsNew_id")
    private Long mWhatsNewId;

    public String getActiveMessage() {
        return mActiveMessage;
    }

    public void setActiveMessage(String activeMessage) {
        mActiveMessage = activeMessage;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDeletestatus() {
        return mDeletestatus;
    }

    public void setDeletestatus(String deletestatus) {
        mDeletestatus = deletestatus;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getEngineSpecification() {
        return mEngineSpecification;
    }

    public void setEngineSpecification(String engineSpecification) {
        mEngineSpecification = engineSpecification;
    }

    public String getGatpartno() {
        return mGatpartno;
    }

    public void setGatpartno(String gatpartno) {
        mGatpartno = gatpartno;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getMrp() {
        return mMrp;
    }

    public void setMrp(String mrp) {
        mMrp = mrp;
    }

    public String getOEPartNo() {
        return mOEPartNo;
    }

    public void setOEPartNo(String oEPartNo) {
        mOEPartNo = oEPartNo;
    }

    public String getProductAttachment() {
        return mProductAttachment;
    }

    public void setProductAttachment(String productAttachment) {
        mProductAttachment = productAttachment;
    }

    public String getProducttype() {
        return mProducttype;
    }

    public void setProducttype(String producttype) {
        mProducttype = producttype;
    }

    public String getSegement() {
        return mSegement;
    }

    public void setSegement(String segement) {
        mSegement = segement;
    }

    public String getVehicle() {
        return mVehicle;
    }

    public void setVehicle(String vehicle) {
        mVehicle = vehicle;
    }

    public Long getWhatsNewId() {
        return mWhatsNewId;
    }

    public void setWhatsNewId(Long whatsNewId) {
        mWhatsNewId = whatsNewId;
    }

}
