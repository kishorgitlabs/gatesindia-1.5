package com.brainmagic.gatescatalog.api.models.scanverify.verifieddata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 */
public class VerifiedData {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private verified data;

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setData(verified data) {
        this.data = data;
    }

    public verified getData() {
        return data;
    }
}