package com.brainmagic.gatescatalog.api.models.threeexpandablelistview;

import java.util.List;

public class ChildModelListModel {

    String model;
    List<SubChildListModel> subChildList;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<SubChildListModel> getSubChildList() {
        return subChildList;
    }

    public void setSubChildList(List<SubChildListModel> subChildList) {
        this.subChildList = subChildList;
    }
}
