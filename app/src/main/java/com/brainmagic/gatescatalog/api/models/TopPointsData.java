package com.brainmagic.gatescatalog.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopPointsData {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("Joining_facebook")
    @Expose
    private Integer joiningFacebook;
    @SerializedName("weekly_quiz")
    @Expose
    private Integer weeklyQuiz;
    @SerializedName("Training_attendance")
    @Expose
    private Integer trainingAttendance;
    @SerializedName("training_assesment")
    @Expose
    private Integer trainingAssesment;
    @SerializedName("Monthly_performer")
    @Expose
    private Integer monthlyPerformer;
    @SerializedName("Car_pasting")
    @Expose
    private Integer carPasting;
    @SerializedName("Flange_install")
    @Expose
    private Integer flangeInstall;
    @SerializedName("Welcome_bonus")
    @Expose
    private String welcomeBonus;
    @SerializedName("tot_points")
    @Expose
    private String totPoints;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getJoiningFacebook() {
        return joiningFacebook;
    }

    public void setJoiningFacebook(Integer joiningFacebook) {
        this.joiningFacebook = joiningFacebook;
    }

    public Integer getWeeklyQuiz() {
        return weeklyQuiz;
    }

    public void setWeeklyQuiz(Integer weeklyQuiz) {
        this.weeklyQuiz = weeklyQuiz;
    }

    public Integer getTrainingAttendance() {
        return trainingAttendance;
    }

    public void setTrainingAttendance(Integer trainingAttendance) {
        this.trainingAttendance = trainingAttendance;
    }

    public Integer getTrainingAssesment() {
        return trainingAssesment;
    }

    public void setTrainingAssesment(Integer trainingAssesment) {
        this.trainingAssesment = trainingAssesment;
    }

    public Integer getMonthlyPerformer() {
        return monthlyPerformer;
    }

    public void setMonthlyPerformer(Integer monthlyPerformer) {
        this.monthlyPerformer = monthlyPerformer;
    }

    public Integer getCarPasting() {
        return carPasting;
    }

    public void setCarPasting(Integer carPasting) {
        this.carPasting = carPasting;
    }

    public Integer getFlangeInstall() {
        return flangeInstall;
    }

    public void setFlangeInstall(Integer flangeInstall) {
        this.flangeInstall = flangeInstall;
    }

    public String getWelcomeBonus() {
        return welcomeBonus;
    }

    public void setWelcomeBonus(String welcomeBonus) {
        this.welcomeBonus = welcomeBonus;
    }

    public String getTotPoints() {
        return totPoints;
    }

    public void setTotPoints(String totPoints) {
        this.totPoints = totPoints;
    }

    public String getInsertdate() {
        return insertdate;
    }


}
