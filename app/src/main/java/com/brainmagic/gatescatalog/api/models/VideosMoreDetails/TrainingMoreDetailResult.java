
package com.brainmagic.gatescatalog.api.models.VideosMoreDetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TrainingMoreDetailResult {

    @SerializedName("Activevideo")
    private String mActivevideo;
    @SerializedName("Country")
    private Object mCountry;
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @SerializedName("VideoAttachment")
    private String mVideoAttachment;
    @SerializedName("VideoCatagoryname")
    private String mVideoCatagoryname;
    @SerializedName("VideoDescription")
    private String mVideoDescription;
    @SerializedName("Video_id")
    private Long mVideoId;
    @SerializedName("videoURL")
    private String mVideoURL;
    @SerializedName("Videoname")
    private String mVideoname;

    public String getActivevideo() {
        return mActivevideo;
    }

    public void setActivevideo(String activevideo) {
        mActivevideo = activevideo;
    }

    public Object getCountry() {
        return mCountry;
    }

    public void setCountry(Object country) {
        mCountry = country;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public String getVideoAttachment() {
        return mVideoAttachment;
    }

    public void setVideoAttachment(String videoAttachment) {
        mVideoAttachment = videoAttachment;
    }

    public String getVideoCatagoryname() {
        return mVideoCatagoryname;
    }

    public void setVideoCatagoryname(String videoCatagoryname) {
        mVideoCatagoryname = videoCatagoryname;
    }

    public String getVideoDescription() {
        return mVideoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        mVideoDescription = videoDescription;
    }

    public Long getVideoId() {
        return mVideoId;
    }

    public void setVideoId(Long videoId) {
        mVideoId = videoId;
    }

    public String getVideoURL() {
        return mVideoURL;
    }

    public void setVideoURL(String videoURL) {
        mVideoURL = videoURL;
    }

    public String getVideoname() {
        return mVideoname;
    }

    public void setVideoname(String videoname) {
        mVideoname = videoname;
    }

}
