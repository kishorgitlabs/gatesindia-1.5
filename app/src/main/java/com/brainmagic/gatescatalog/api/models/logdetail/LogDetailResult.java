
package com.brainmagic.gatescatalog.api.models.logdetail;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LogDetailResult {

    @SerializedName("Id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("Product")
    private String mProduct;
    @SerializedName("Recordname")
    private String mRecordname;
    @SerializedName("UserLogin")
    private String mUserLogin;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getProduct() {
        return mProduct;
    }

    public void setProduct(String product) {
        mProduct = product;
    }

    public String getRecordname() {
        return mRecordname;
    }

    public void setRecordname(String recordname) {
        mRecordname = recordname;
    }

    public String getUserLogin() {
        return mUserLogin;
    }

    public void setUserLogin(String userLogin) {
        mUserLogin = userLogin;
    }

}
