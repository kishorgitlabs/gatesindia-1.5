
package com.brainmagic.gatescatalog.api.models.background;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Backgroundpojo {

    @SerializedName("data")
    private BackgroundData mData;
    @SerializedName("result")
    private String mResult;

    public BackgroundData getData() {
        return mData;
    }

    public void setData(BackgroundData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
