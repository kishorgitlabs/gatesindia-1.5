package com.brainmagic.gatescatalog.api.models.threeexpandablelistview;

public class SubChildListModel {
    String stroke, cc, Kw, monthTill, yearTill;

    public String getStroke() {
        return stroke;
    }

    public void setStroke(String stroke) {
        this.stroke = stroke;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getKw() {
        return Kw;
    }

    public void setKw(String kw) {
        Kw = kw;
    }

    public String getMonthTill() {
        return monthTill;
    }

    public void setMonthTill(String monthTill) {
        this.monthTill = monthTill;
    }

    public String getYearTill() {
        return yearTill;
    }

    public void setYearTill(String yearTill) {
        this.yearTill = yearTill;
    }
}
