
package com.brainmagic.gatescatalog.api.models.webotpverify;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class WebOTPVerify {

    @SerializedName("data")
    private OTPVerifyData mData;
    @SerializedName("result")
    private String mResult;

    public OTPVerifyData getData() {
        return mData;
    }

    public void setData(OTPVerifyData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
