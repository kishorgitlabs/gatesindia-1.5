
package com.brainmagic.gatescatalog.api.models.distributors;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DistributorListResult {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("AddressProofPhoto")
    private Object mAddressProofPhoto;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Contact")
    private Double mContact;
    @SerializedName("contactperson")
    private String mContactperson;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("CreatedBy")
    private String mCreatedBy;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("DistributorKey")
    private String mDistributorKey;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("GatesId")
    private String mGatesId;
    @SerializedName("IDProofPhoto")
    private Object mIDProofPhoto;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Landline")
    private String mLandline;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("photo")
    private String mPhoto;
    @SerializedName("Pincode")
    private Double mPincode;
    @SerializedName("ShopImage")
    private String mShopImage;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdatedBy")
    private String mUpdatedBy;
    @SerializedName("UpdatedDate")
    private String mUpdatedDate;
    @SerializedName("UserType")
    private Object mUserType;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Object getAddressProofPhoto() {
        return mAddressProofPhoto;
    }

    public void setAddressProofPhoto(Object addressProofPhoto) {
        mAddressProofPhoto = addressProofPhoto;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public Double getContact() {
        return mContact;
    }

    public void setContact(Double contact) {
        mContact = contact;
    }

    public String getContactperson() {
        return mContactperson;
    }

    public void setContactperson(String contactperson) {
        mContactperson = contactperson;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getCreatedBy() {
        return mCreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        mCreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getDistributorKey() {
        return mDistributorKey;
    }

    public void setDistributorKey(String distributorKey) {
        mDistributorKey = distributorKey;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getGatesId() {
        return mGatesId;
    }

    public void setGatesId(String gatesId) {
        mGatesId = gatesId;
    }

    public Object getIDProofPhoto() {
        return mIDProofPhoto;
    }

    public void setIDProofPhoto(Object iDProofPhoto) {
        mIDProofPhoto = iDProofPhoto;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLandline() {
        return mLandline;
    }

    public void setLandline(String landline) {
        mLandline = landline;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public Double getPincode() {
        return mPincode;
    }

    public void setPincode(Double pincode) {
        mPincode = pincode;
    }

    public String getShopImage() {
        return mShopImage;
    }

    public void setShopImage(String shopImage) {
        mShopImage = shopImage;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUpdatedBy() {
        return mUpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        mUpdatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        mUpdatedDate = updatedDate;
    }

    public Object getUserType() {
        return mUserType;
    }

    public void setUserType(Object userType) {
        mUserType = userType;
    }

}
