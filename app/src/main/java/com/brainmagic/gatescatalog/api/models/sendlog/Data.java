package com.brainmagic.gatescatalog.api.models.sendlog;

import java.io.Serializable;

public class Data implements Serializable {
	private int Id;
	private String Segment;
	private String Partno;
	private String VehicleModel;
	private String Name;
	private String Mobileno;
	private String Usertype;
	private String Devicetype;
	private String State;
	private String City;
	private String Location;
	private String Zipcode;
	private String Insertdate;
	private String insertdate1;


	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getSegment() {
		return Segment;
	}

	public void setSegment(String segment) {
		Segment = segment;
	}

	public String getPartno() {
		return Partno;
	}

	public void setPartno(String partno) {
		Partno = partno;
	}

	public String getVehicleModel() {
		return VehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		VehicleModel = vehicleModel;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getMobileno() {
		return Mobileno;
	}

	public void setMobileno(String mobileno) {
		Mobileno = mobileno;
	}

	public String getUsertype() {
		return Usertype;
	}

	public void setUsertype(String usertype) {
		Usertype = usertype;
	}

	public String getDevicetype() {
		return Devicetype;
	}

	public void setDevicetype(String devicetype) {
		Devicetype = devicetype;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getZipcode() {
		return Zipcode;
	}

	public void setZipcode(String zipcode) {
		Zipcode = zipcode;
	}

	public String getInsertdate() {
		return Insertdate;
	}

	public void setInsertdate(String insertdate) {
		Insertdate = insertdate;
	}

	public String getInsertdate1() {
		return insertdate1;
	}

	public void setInsertdate1(String insertdate1) {
		this.insertdate1 = insertdate1;
	}

	@Override
	public String toString() {
		return "Data{" +
				"Id=" + Id +
				", Segment='" + Segment + '\'' +
				", Partno='" + Partno + '\'' +
				", VehicleModel='" + VehicleModel + '\'' +
				", Name='" + Name + '\'' +
				", Mobileno='" + Mobileno + '\'' +
				", Usertype='" + Usertype + '\'' +
				", Devicetype='" + Devicetype + '\'' +
				", State='" + State + '\'' +
				", City='" + City + '\'' +
				", Location='" + Location + '\'' +
				", Zipcode='" + Zipcode + '\'' +
				", Insertdate='" + Insertdate + '\'' +
				", insertdate1='" + insertdate1 + '\'' +
				'}';
	}
}