package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.gatescatalog.gates.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.viewHolder> {
    private static final String TAG = "RecyclerViewAdapter";
    List<String> productImageList;
//    private List<LogDetailResult> mImage;
    private Context context;
    int selected_position=0,prePosition=-1;
//    private  Integer Imageurl;
    private GetImage click;
//    private boolean mainImageInfo=true;


    public ImageAdapter(Context context, List<String> productImageList) {
        this.context = context;
        this.productImageList = productImageList;
//        this.click = (GetImage) context;

    }


    //    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        View view = (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem,viewGroup,false);
//        return new viewHolder(view);
//    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layoutlistitem,viewGroup,false);

        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final viewHolder viewHolder, final int i) {
        /*Glide.with(context)
                .load(mImage.get(i))
                .asBitmap()
                .into(viewHolder.imageView);*/

        Picasso.Builder builder = new Picasso.Builder(context);
        builder.listener(new Picasso.Listener()
        {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
            {
                exception.printStackTrace();
            }
        });
        builder.build().load(productImageList.get(i)).error(R.drawable.noimages).into(viewHolder.imageView);
//        viewHolder.imageView.setImageResource(mImage.get(i).getThumbnail_Image());
        if(prePosition!=i)
            viewHolder.itemView.setBackgroundColor(Color.WHITE);

        //viewHolder.imageView.setBackgroundColor(Color.parseColor("#000000"));

        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    click.setOnImageClick(productImageList.get(i),i);
                    viewHolder.itemView.setBackgroundColor(Color.RED);
//                    if(mainImageInfo)
//                    {
//                    mainImageInfo=false;
//                        Toast.makeText(context,"Swipe down to reload the Main Image",Toast.LENGTH_LONG).show();
//                    }

                notifyDataSetChanged();
                prePosition=i;
            }
        });
    }


    @Override
    public int getItemCount() {
        return productImageList.size();
    }

    //    @Override
//    public void onBindViewHolder(viewHolder viewHolder, int i) {
//        Glide.with(context)
//                .load(mImage.get(i))
//                .into(viewHolder.imageView);
//    }
    public class viewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,AdapterImage;


        public viewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            AdapterImage = (ImageView)itemView.findViewById(R.id.imageadapter);


        }

//        @Override
//        public void onClick(View v) {
//          /*  if (getAdapterPosition() == RecyclerView.NO_POSITION) return;
//
//            // Updating old as well as new positions
//            notifyItemChanged(selected_position);
//            selected_position = getLayoutPosition();
//
//            notifyItemChanged(selected_position);
//*/
//            if(click != null){
//                int position = getAdapterPosition();
//                if(position != RecyclerView.NO_POSITION){
//                    click.setOnImageClick(position);
//
//                }
//            }
//        }
    }

    public void setImageClickListener(GetImage getImage)
    {
        this.click = getImage;
    }

    public interface GetImage{
        void setOnImageClick(String image, int pos);
    }

}
