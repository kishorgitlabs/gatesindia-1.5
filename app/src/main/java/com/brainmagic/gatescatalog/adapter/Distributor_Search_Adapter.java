package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.brainmagic.gatescatalog.Constants.ROOT_URL;

/**
 * Created by Brainmagic 02 on 9/9/2016.
 */
public class Distributor_Search_Adapter extends ArrayAdapter {

    Context context;
    List<String> data;
    ArrayList<Bitmap> list_logo;


    public Distributor_Search_Adapter(Context context, List<String> data) {
        super(context, R.layout.distributor_adapter, data);

        this.context = context;
        this.data = data;
//        this.list_logo=list_logo;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ContactHolder holder;

        convertView=null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.distributor_adapter, parent, false);

            holder = new ContactHolder();
            TextView contact_key = (TextView) convertView.findViewById(R.id.contact_key);
            ImageView contact_image=(ImageView) convertView.findViewById(R.id.contact_image);

            contact_key.setText(data.get(position));
            Picasso.with(context).load(ROOT_URL + "Uploads/Events/"+data.get(position)).error(R.drawable.noimages).into(contact_image);
            contact_image.setImageBitmap(list_logo.get(position));


            convertView.setTag(holder);
        } else {
            holder = (ContactHolder) convertView.getTag();
        }


        return convertView;
    }

}


class ContactHolder {
    public TextView sno;
    public TextView orderno;
    public TextView datetxt;
    public TextView delarname;


}
