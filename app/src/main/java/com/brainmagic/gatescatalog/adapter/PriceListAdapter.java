package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.api.models.PriceList.PriceLists;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class PriceListAdapter extends ArrayAdapter{


    Context context;
    List<PriceLists>items;



    public PriceListAdapter(@NonNull Context context,List<PriceLists>items) {
        super(context, R.layout.price);
        this.context=context;
        this.items=items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        View view= LayoutInflater.from(context).inflate(R.layout.pricelists,parent,false);
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.price,parent,false);

        TextView one=view.findViewById(R.id.part_nos);
        TextView two=view.findViewById(R.id.articel_nos);
        TextView three=view.findViewById(R.id.mrpa);

        one.setText(items.get(position).getPartno());
        two.setText(items.get(position).getCatalogPartnumber());

        three.setText("र  "+items.get(position).getMRP());

        return view;
    }

    @Override
    public int getCount() {
        return items.size();

    }
}
