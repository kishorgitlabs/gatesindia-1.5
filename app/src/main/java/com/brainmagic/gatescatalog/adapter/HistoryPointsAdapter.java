package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class HistoryPointsAdapter extends ArrayAdapter {


    Context context;

    public HistoryPointsAdapter(@NonNull Context context) {
        super(context, R.layout.historypointsadapter);
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.historypointsadapter, parent, false);
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return 100;
    }
}

