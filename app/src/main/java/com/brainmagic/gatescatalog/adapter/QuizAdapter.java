package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.brainmagic.gatescatalog.gates.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class QuizAdapter extends ArrayAdapter {

    Context context;

//    private OnRadioChangeListener onRadioChangeListener;
    ArrayList<String> answersStringArray= new ArrayList<String>();
    public QuizAdapter(@NonNull Context context) {
        super(context, R.layout.quizadapter);
        this.context = context;
//        this.onRadioChangeListener = onRadioChangeListener;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.quizadapter, parent, false);
        }
//        RadioGroup group = (RadioGroup) convertView.findViewById(R.id.radiogroup);

        TextView text=(TextView)convertView.findViewById(R.id.text);
        View finalConvertView1 = convertView;
//        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//
//                RadioButton radioButton = (RadioButton) finalConvertView1.findViewById(group.getCheckedRadioButtonId());
//                String ansText = radioButton.getText().toString();
//                answersStringArray.add(ansText);
//
////
//                String options=answersStringArray.get(position);
//                Intent intent = new Intent("custom-message");
//                intent.putExtra("item",  options);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//
//
//            }
//        });


        return convertView;
    }

    @Override
    public int getCount() {

        return 20;
    }
//    interface OnRadioChangeListener{
//        void onRadioChange(RadioGroup radioGroup,int checkedId);
//    }
}
