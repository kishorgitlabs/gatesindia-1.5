package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class EngineDetailsAdapter extends ArrayAdapter<String> {

    private Context context;
    private Integer VelCheck;
    private CheckBox checkBox;

    private boolean isModelCheck;
    private List<String> selectModelList;
    private Map<String, Boolean> isCheck;
    private ArrayList<String> gates_part_list = new ArrayList<>();
    private ArrayList<String>
            partDescriptionList, appAttList, strokeList, equipmentDateFromList, equipmentDateToList, moreNewDetailCount;
    String oemstng, modelstng, modelcodestng, enginecodestng;
    private List<EngineCodeResult> data;
//    private PdfForamt pdfGenerator;
//    private List<Partnumber> modelList;
//    private HashMap<String, Partnumber> hashMap;
//    private RelativeLayout evenSpace;


    public EngineDetailsAdapter(Context context, List<EngineCodeResult> data) {
        super(context, R.layout.adapter_product_details);
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        DetailsHolder holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_product_details, parent, false);

            holder = new DetailsHolder();
            holder.enginecodetxt = (TextView) convertView.findViewById(R.id.enginecodetxt);
            holder.newCount = (TextView) convertView.findViewById(R.id.cart_badge_product_detail);
            holder.partdescetxt = (TextView) convertView.findViewById(R.id.partdescetxt);
            holder.partnotxt = (TextView) convertView.findViewById(R.id.partnotxt);
//            holder.evenSpace = (RelativeLayout) convertView.findViewById(R.id.even_space);

            checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);

            holder.appTT = (TextView) convertView.findViewById(R.id.app_att);
//            holder.eqip2txt = (TextView) convertView.findViewById(R.id.eqip2txt);
           // holder.eqipfdatetxt = (TextView) convertView.findViewById(R.id.eqipfdatetxt);
           // holder.eqiptdatetxt = (TextView) convertView.findViewById(R.id.eqiptdatetxt);
          //  holder.more_info = (ImageView) convertView.findViewById(R.id.more_info);
            holder.enginecodetxt.setText(data.get(position).getEnginecode());
            holder.partdescetxt.setText(data.get(position).getStatusDescription());
            holder.partnotxt.setText(data.get(position).getArticle());

//            if (Product_Details_Activity.isActionmode) {
//                checkBox.setVisibility(View.VISIBLE);
//                holder.newCount.setVisibility(View.GONE);
//                evenSpace.setVisibility(View.VISIBLE);
////                if (VelCheck == 1)
//                if(selectModelList!=null)
//                    if(selectModelList.contains(gates_part_list.get(position)))
//                    {
//                        checkBox.setChecked(true);
//                        if (VelCheck == 1)
//                        {
//                            modelList.clear();
//                            for (String partName : gates_part_list) {
//                                    Partnumber model = new Partnumber();
//                                    model.setPartnumberpdf(partName);
//                                    modelList.add(model);
//                                    hashMap.put(partName,model);
//                            }
//                            selectModel.selectedModel(modelList);
//                            VelCheck++;
//                        }
////                        selectModelList.addAll(gates_part_list);
//
//                    }
//                    else {
//                        checkBox.setChecked(false);
//                        if(VelCheck==-1) {
//                            modelList.clear();
//                            hashMap.clear();
//                            VelCheck--;
//                        }
//                        selectModel.selectedModel(modelList);
//                    }
//
//            } else
//                {
//                    checkBox.setVisibility(View.GONE);
//                    evenSpace.setVisibility(View.GONE);
//            }
//
//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    int positioin = (int) buttonView.getTag();
//
//                    if (isChecked) {
//
//                        Product_Details_Activity.userSelection.remove(gates_part_list.get(positioin));
//                        Partnumber models = new Partnumber();
//                        models.setPartnumberpdf(gates_part_list.get(positioin));
//                        isCheck.put(gates_part_list.get(positioin), isChecked);
//                        modelList.add(models);
//                        hashMap.put(gates_part_list.get(positioin), models);
//                        selectModelList.add(gates_part_list.get(positioin));
//
//                    } else {
//                        Product_Details_Activity.userSelection.add(gates_part_list.get(positioin));
//                        isCheck.remove(gates_part_list.get(positioin));
//                        modelList.remove(hashMap.get(gates_part_list.get(positioin)));
//                        hashMap.remove(gates_part_list.get(positioin));
//                        selectModelList.remove(gates_part_list.get(positioin));
//                    }
//                    selectModel.selectedModel(modelList);
//                }
//
//            });

//            String text="";
//            if(!TextUtils.isEmpty(equipment2List.get(position))&&!TextUtils.isEmpty(equipment1List.get(position))) {
//                text = equipment1List.get(position) + "/" + equipment2List.get(position);
//            } else if(!TextUtils.isEmpty(equipment1List.get(position))) {
//                text = equipment1List.get(position)+"/";
//            } else if(!TextUtils.isEmpty(equipment2List.get(position))) {
//                text = "/"+ equipment2List.get(position);
//            } else {
//                text="";
//            }

//            holder.appTT.setText(text);
//            holder.eqip2txt.setText(equipment2List.get(position));
            holder.appTT.setText(data.get(position).getApplicationAttributes());

            holder.enginecodetxt.setPaintFlags(holder.enginecodetxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);





//            holder.eqip2txt.setText(equipment2List.get(position));
//            holder.eqipfdatetxt.setText(equipmentDateFromList.get(position));
//      holder.eqiptdatetxt.setText(equipmentDateToList.get(position));



         /*   holder.more_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent a = new Intent(context, Product_More_Details_Activity.class);

                    a.putExtra("MODEL_CODE", modelcodestng);
                    a.putExtra("MODEL", modelstng);
                    a.putExtra("OEM", oemstng);

                    a.putExtra("PARTDESC", partDescriptionList.get(position));
                    a.putExtra("GATESPART", gates_part_list.get(position));

                    a.putExtra("EQUIPMENT1", equipment1List.get(position));
                    a.putExtra("EQUIPMENT2", equipment2List.get(position));
                    a.putExtra("EQUIPMENTDATEF", equipmentDateFromList.get(position));
                    a.putExtra("EQUIPMENTDATET", equipmentDateToList.get(position));

                    context.startActivity(a);
                }
            });*/


            convertView.setTag(holder);
        } else {
            holder = (DetailsHolder) convertView.getTag();
        }

        return convertView;
    }




    class DetailsHolder {
        TextView partdescetxt, enginecodetxt;
        //    TextView eqip2txt;
        TextView appTT;
        TextView partnotxt, eqipfdatetxt, eqiptdatetxt, newCount;
        ImageView more_info;
//        RelativeLayout evenSpace;


    }


}
