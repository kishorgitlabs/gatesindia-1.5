package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.schemespromo.SchemesPromoResult;

import java.util.List;

public class SchemesPromoAdapter extends ArrayAdapter {
    private Context context;
    private  List<SchemesPromoResult> data;
    public SchemesPromoAdapter(@NonNull Context context, List<SchemesPromoResult> data) {
        super(context, R.layout.schemes_promo_adapter);
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.schemes_promo_adapter,parent,false);

        }

        TextView schemesName = convertView.findViewById(R.id.schemes_name);
        TextView desc = convertView.findViewById(R.id.schemes_desc);
        TextView expDate = convertView.findViewById(R.id.schemes_exp_date);

        try {
            schemesName.setText(data.get(position).getSchemesname());
            desc.setText(data.get(position).getDescription());
            String defDate = data.get(position).getShemeExpiryDate();
            StringBuilder tempDate =  new StringBuilder();
            if(defDate.contains("T"))
            {
                String[] tempDateArray = defDate.split("T");
                tempDate.append(tempDateArray[0]);
            }
            else {
                tempDate.append(data.get(position).getShemeExpiryDate());
            }
            expDate.setText(tempDate);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
