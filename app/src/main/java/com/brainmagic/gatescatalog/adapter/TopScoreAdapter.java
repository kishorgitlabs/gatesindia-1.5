package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.api.models.TopPointsData;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class TopScoreAdapter extends ArrayAdapter {
    private Context context;
    private List<TopPointsData> data;
    public TopScoreAdapter(@NonNull Context context, List<TopPointsData> data) {
        super(context, R.layout.tophistorypoints);
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.tophistorypoints,parent,false);
        }

        TextView sno = convertView.findViewById(R.id.sno);
        TextView name = convertView.findViewById(R.id.name);
        TextView scores = convertView.findViewById(R.id.scores);
         TextView statecity=convertView.findViewById(R.id.statecity);

//        .setText(data.get(position).getStroke()+"");
      name  .setText(data.get(position).getName());
        scores.setText(data.get(position).getTotPoints());
sno.setText(position+1+"");
statecity.setText(data.get(position).getState()+"/"+ data.get(position).getCity());



        return convertView;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
