package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.gatescatalog.api.models.QuizGetClass;
import com.brainmagic.gatescatalog.gates.R;

import java.util.ArrayList;
import java.util.List;

public class QuizUpdateAdapter extends RecyclerView.Adapter<QuizUpdateAdapter.ViewHolder> {

    Context context;
    List<QuizGetClass> data ;
    List getArrayList;
    public QuizUpdateAdapter(Context context, List<QuizGetClass> data, List getArrayList) {
        this.context = context;
        this.data = data;
        this.getArrayList=getArrayList;

    }
    @Override
    public QuizUpdateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.quizupdateadapter, parent, false);
        QuizUpdateAdapter.ViewHolder viewHolder = new QuizUpdateAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

holder.questionno.setText(data.get(position).getQuestionno());
holder.question.setText(data.get(position).getQuestionE());
holder.correctanswer.setText(data.get(position).getCorrectanswer());
holder.hindiquestion.setText(data.get(position).getQuestionH());


    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        TextView questionno,question,correctanswer,hindiquestion;
        public ViewHolder(View itemView) {
            super(itemView);

       question=itemView.findViewById(R.id.question);
            questionno=itemView.findViewById(R.id.questionno);
            correctanswer=itemView.findViewById(R.id.correctanswer);
            hindiquestion=itemView.findViewById(R.id.hindiquestion);




        }
    }




}
