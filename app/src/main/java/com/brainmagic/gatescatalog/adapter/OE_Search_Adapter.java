package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.brainmagic.gatescatalog.gates.R;

import java.util.ArrayList;


public class OE_Search_Adapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList<String> supplierList, part_desc_list, part_number_list;

    public OE_Search_Adapter(Context context, ArrayList<String> supplierList, ArrayList<String> part_desc_list, ArrayList<String> part_number_list) {
        super(context, R.layout.adapter_oe_search_, part_desc_list);

        this.context = context;
        this.supplierList = supplierList;
        this.part_desc_list = part_desc_list;
        this.part_number_list = part_number_list;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        OEHolder holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_oe_search_, parent, false);

            holder = new OEHolder();
//            holder.maketxt = (TextView) convertView.findViewById(R.id.maketxt);
            holder.supplier = (TextView) convertView.findViewById(R.id.supplier);
            holder.decesptxt = (TextView) convertView.findViewById(R.id.decesptxt);
            holder.gatepartnotxt = (TextView) convertView.findViewById(R.id.gatepartnotxt);


           // holder.gatepartnotxt.setPaintFlags(holder.gatepartnotxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

//            holder.maketxt.setText(vechicle_name_list.get(position));
            holder.supplier.setText(supplierList.get(position));
            holder.decesptxt.setText(part_desc_list.get(position));
            holder.gatepartnotxt.setText(part_number_list.get(position));


           /* holder.gatepartnotxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  *//*  final Dialog settingsDialog = new Dialog(context);
                   // settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    View view =((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.image_layout
                            , null);
                    settingsDialog.setContentView(view);
                    Button okay = (Button)view.findViewById(R.id.okay);
                    ImageView zoom = (ImageView)view.findViewById(R.id.zoomimage);

                    Picasso.with(context).load("file:///android_asset/" + part_desc_list.get(position).trim() + ".jpg").into(zoom);

                    settingsDialog.show();
                    okay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            settingsDialog.dismiss();
                        }
                    });*//*


                   *//* if (part_desc_list.get(position).equals("")) {

                        Toast.makeText(context, part_desc_list.get(position) + " Image not found ", Toast.LENGTH_LONG).show();

                    } else {
                        Intent goZoom = new Intent(context, ZoomActivity.class);
                        goZoom.putExtra("ImageName", ImageName_List.get(position));
                        goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        context.startActivity(goZoom);
                    }*//*


                }
            });*/


            convertView.setTag(holder);
        } else {
            holder = (OEHolder) convertView.getTag();
        }


        return convertView;
    }

}


class OEHolder {
    TextView maketxt;
    TextView supplier;
    TextView decesptxt;
    TextView gatepartnotxt;
}

