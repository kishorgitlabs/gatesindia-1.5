package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;


import com.brainmagic.gatescatalog.api.models.QuizGetClass;

import com.brainmagic.gatescatalog.gates.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

public class QuizRecyclerAdapter extends RecyclerView.Adapter<QuizRecyclerAdapter.ViewHolder> {

    Context context;
    List<QuizGetClass>  data ;
    public static ArrayList<String> selectedAnswers,Selectedsno,Selectedquestion;
    String question,sno;


    public QuizRecyclerAdapter(Context context, List<QuizGetClass> data) {
        this.context = context;
        this.data = data;
        selectedAnswers = new ArrayList<>();
        Selectedsno = new ArrayList<>();
        Selectedquestion = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            selectedAnswers.add("Not Attempted");
        }
        for (int i = 0; i < data.size(); i++) {
            Selectedsno.add("Not Attempted");
        }
        for (int i = 0; i < data.size(); i++) {
            Selectedquestion.add("Not Attempted");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.quizadapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.qno.setText(data.get(position).getQuestionno());
        holder.questioneng.setText(data.get(position).getQuestionE());
        holder.questionhin.setText(data.get(position).getQuestionH());
        holder.yes.setText(data.get(position).getOptionAE()+"["+data.get(position).getOptionAH()+"]");
        holder.no.setText(data.get(position).getOptionBE()+"["+data.get(position).getOptionBH()+"]");
        holder.none.setText(data.get(position).getOptionCE()+"["+data.get(position).getOptionCH()+"]");

      question=data.get(position).getQuestionE();

       holder. yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                selectedAnswers.set(position, data.get(position).getQuestionno()+"."+data.get(position).getOptionAE()+"sno"+question);
                    Selectedsno.set(position, data.get(position).getQuestionno());
                    Selectedquestion.set(position, question);
                    selectedAnswers.set(position, data.get(position).getOptionAE());
                }

            }
        });
    holder.no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//       selectedAnswers.set(position, data.get(position).getQuestionno()+data.get(position).getOptionBE()+"."+ question);

                    Selectedsno.set(position, data.get(position).getQuestionno());
                    Selectedquestion.set(position, question);
                    selectedAnswers.set(position, data.get(position).getOptionBE());
                }

            }
        });
        holder.none.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//            selectedAnswers.set(position, data.get(position).getQuestionno()+data.get(position).getOptionCE()+"."+question);

                    Selectedsno.set(position, data.get(position).getQuestionno());
                    Selectedquestion.set(position, question);
                    selectedAnswers.set(position, data.get(position).getOptionCE());
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

            TextView qno,questioneng,questionhin;
            RadioGroup radiogroup;
            RadioButton yes,no,none;
            RadioButton radioButton;
            int id;
        public ViewHolder(View itemView) {
            super(itemView);



            this.qno=(TextView)itemView.findViewById(R.id.qno);
            this.questioneng=(TextView)itemView.findViewById(R.id.questioneng);
            this.questionhin=(TextView)itemView.findViewById(R.id.questionhin);

            radiogroup=(RadioGroup)itemView.findViewById(R.id.radiogroup);
            id=radiogroup.getCheckedRadioButtonId();
            radioButton=(RadioButton) itemView.findViewById(id);
            yes=(RadioButton)itemView.findViewById(R.id.yes);
            no=(RadioButton)itemView.findViewById(R.id.no);
            none=(RadioButton)itemView.findViewById(R.id.none);


        }
    }


}
